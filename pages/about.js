import React, { useState } from "react";
import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { getPerson } from "lib/people";
import { getInstitution } from "lib/institutions";
import ReCAPTCHA from "react-google-recaptcha";
import path from "path";
import fs from "fs";
import dynamic from "next/dynamic";
import TeamListItem from "components/TeamListItem";
import { t } from "lib/localization/localization";
import styles from "styles/About.module.css";

export default function About({
  rtl,
  licence,
  founders,
  colaborators,
  webdevelopers,
}) {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale } = router;

  // Load introduction (which is inside an MDX file) using Dynamic Import:
  // - We use MDX files for the intro so we can use components inside them;
  // - https://nextjs.org/docs/advanced-features/dynamic-import
  // - https://andersdjohnson.com/posts/nextjs-static-mdx-dynamic-routes-metadata
  const Intro = dynamic(() => import(`/content/${locale}/about/intro.mdx`));

  // Let's create the list of founders, colaborators & webdevelopers:
  const foundersList = founders.map((founder) => {
    return <TeamListItem key={founder.slug} item={founder} />;
  });
  const colaboratorsList = colaborators.map((colaborator) => {
    return <TeamListItem key={colaborator.slug} item={colaborator} />;
  });
  const webdevelopersList = webdevelopers.map((webdeveloper) => {
    return <TeamListItem key={webdeveloper.slug} item={webdeveloper} />;
  });

  // Function that prepares the data for POST body:
  const encode = (data) => {
    return Object.keys(data)
      .map(
        (key) => encodeURIComponent(key) + "=" + encodeURIComponent(data[key])
      )
      .join("&");
  };

  // States for the form fields:
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  // Function that updates the fields states as the user fill the fields:
  const handleParam = (setValue) => (e) => setValue(e.target.value);

  // Function that enables or disables the form button based on captcha component events:
  function onRecaptchaChange() {
    const button = document.getElementById("formSubmit");
    button.classList.toggle("enabled");
    button.classList.toggle("disabled");
  }

  // Submit form handler:
  // - It must be async because we'll have to wait for the captcha validation.
  const handleSubmit = async (e) => {
    // - preventDefault prevents the form redirecting the page after submit.
    e.preventDefault();
    // If captcha is checked, we proceed:
    if (grecaptcha.getResponse() !== "") {
      // Let's indicate on the button that the site is processing the form (actually, we are waiting the captcha validation):
      const button = document.getElementById("formSubmit");
      button.classList.remove("enabled");
      button.classList.add("working");
      // Let's validate the captcha with our serverless function on AWS:
      const grecaptchaResponse = grecaptcha.getResponse();
      const validateReCaptchaLambdaURL = `https://ip2ej777q3.execute-api.sa-east-1.amazonaws.com/default/validateReCaptcha?site=tuimp&response=${grecaptchaResponse}`;
      const captchaVerification = await fetch(validateReCaptchaLambdaURL, {
        method: "POST",
      });
      const captchaVerificationResult = await captchaVerification.json();
      // If the result is true, then we post the form on Netlify and route to the message_received page:
      if (captchaVerificationResult) {
        fetch("/", {
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
          body: encode({
            "form-name": "contact",
            subject: "TUIMP - Contact form message",
            name: name,
            location: location,
            email: email,
            message: message,
            language: locale,
          }),
        })
          .then(() => router.push("message_received"))
          .catch((error) => alert(error));
        // If not, we just reset the captcha and the button:
      } else {
        grecaptcha.reset();
        button.classList.remove("working");
        button.classList.add("disabled");
      }
    }
  };

  return (
    <>
      <main className={"content " + (rtl ? "rtl" : "ltr")} id={styles.about}>
        <NextSeo
          title={"TUIMP | " + t(locale, "pages-about", "pages-about-title")}
        />

        <section>
          <h1>{t(locale, "pages-about", "pages-about-title")}:</h1>

          <Intro />
        </section>

        <section>
          <h1>{t(locale, "pages-about", "pages-about-team")}:</h1>

          <h2>{t(locale, "pages-about", "pages-about-founders")}:</h2>
          <ul>{foundersList}</ul>

          <h2>{t(locale, "pages-about", "pages-about-collaborators")}:</h2>
          <ul>{colaboratorsList}</ul>

          <h2>{t(locale, "pages-about", "pages-about-webdeveloper")}:</h2>
          <ul>{webdevelopersList}</ul>
        </section>

        <section>
          <h1>{t(locale, "pages-about", "pages-about-contact")}:</h1>

          <p>{t(locale, "pages-about", "pages-about-contact-intro")}</p>

          <form onSubmit={handleSubmit}>
            <input
              type="hidden"
              id="language"
              name="language"
              value={locale}
            ></input>

            <label>
              {t(locale, "pages-about", "pages-about-contact-form-name")}:
              <input
                type="text"
                name="name"
                required
                value={name}
                onChange={handleParam(setName)}
              />
            </label>

            <label>
              {t(locale, "pages-about", "pages-about-contact-form-location")}:
              <input
                type="text"
                name="location"
                required
                value={location}
                onChange={handleParam(setLocation)}
              />
            </label>

            <label>
              {t(locale, "pages-about", "pages-about-contact-form-email")}:
              <input
                type="email"
                name="email"
                required
                value={email}
                onChange={handleParam(setEmail)}
              />
            </label>

            <label>
              {t(locale, "pages-about", "pages-about-contact-form-message")}:
              <textarea
                name="message"
                required
                value={message}
                onChange={handleParam(setMessage)}
              ></textarea>
            </label>

            <div id="recaptcha">
              <ReCAPTCHA
                size="normal"
                onChange={onRecaptchaChange}
                sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
              />
            </div>

            <button id="formSubmit" className="disabled" type="submit">
              <span>
                {t(locale, "pages-about", "pages-about-contact-form-submit")}
              </span>
            </button>
          </form>
        </section>

        <section>
          <h1>{t(locale, "pages-about", "pages-about-licence")}:</h1>

          <p>{licence}</p>

          <a
            target="_blank"
            rel="noreferrer"
            href="http://creativecommons.org/licenses/by-nc-sa/4.0/"
          >
            <img
              alt="Creative Commons License"
              src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"
            />
          </a>
        </section>
      </main>
    </>
  );
}

export async function getStaticProps(context) {
  // Let's create the path of the licence file...
  const licencePath = path.join(
    "content",
    `${context.locale}`,
    "about",
    "licence.txt"
  );
  // ... and read its content to send to our component:
  // - https://nodejs.org/api/fs.html#fsreadfilesyncpath-options
  const licence = fs.readFileSync(licencePath, "utf8");

  // Let's create arrays of founders, colaborators & webdevelopers:
  // - We do this manually because we need to show these lists on the website in the exact orders below;
  // - To each item we add the type information, so that the component responsible for rendering these itens can do it correctly.
  const founders = [
    {
      ...(await getPerson(context.locale, "grazyna_stasinska")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "natalia_vale_asari")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "stanley_kurtz")),
      ...{ type: "person" },
    },
  ];
  const colaborators = [
    {
      ...(await getPerson(context.locale, "gloria_delgado_inglada")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "dorota_koziel_wierzbowska")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "mimoza_hafizi")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "haik_harutyunian")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "john_seiradakis")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "jean_schneider")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "valentina_luridiana_galati")),
      ...{ type: "person" },
    },
    {
      ...(await getInstitution(context.locale, "sirius_astronomy_association")),
      ...{ type: "institution" },
    },
    {
      ...(await getPerson(context.locale, "zakaria_meliani")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "gina_theodoropoulou")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "monica_rodriguez")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "anna_wojtowicz")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "natalia_zywucka")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "francoise_combes")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "christiane_vilain")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "georges_alecian")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "mirjana_povic")),
      ...{ type: "person" },
    },
    {
      ...(await getInstitution(context.locale, "yunivarsitii_jimmaa")),
      ...{ type: "institution" },
    },
    {
      ...(await getPerson(context.locale, "eric_nhlanhla_mbambo")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "bogdan_tofanica")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "marina_trevisan")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "rogerio_riffel")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "gina_panopoulou")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "julieta_fierro")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "marcello_fulchignoni")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "catarina_aydar")),
      ...{ type: "person" },
    },
    {
      ...(await getInstitution(context.locale, "universita_di_padova")),
      ...{ type: "institution" },
    },
    {
      ...(await getPerson(context.locale, "paola_moreira_delgado")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "frederic_vicent")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "meline_asryan")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "stavros_akras")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "maria_eduarda_ramos_pedro")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "jerome_novak")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "sylvia_ekstrom")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "alexandre_le_tiec")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "laura_bernard")),
      ...{ type: "person" },
    },
    {
      ...(await getPerson(context.locale, "eesha_das_gupta")),
      ...{ type: "person" },
    },
  ];
  const webdevelopers = [
    {
      ...(await getPerson(context.locale, "fabricio_chiquio_boppre")),
      ...{ type: "person" },
    },
  ];

  return {
    props: {
      licence: licence,
      founders: founders,
      colaborators: colaborators,
      webdevelopers: webdevelopers,
    },
  };
}
