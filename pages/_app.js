import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import Script from "next/script";
import Masthead from "components/Masthead";
import IncompatibleBrowser from "components/IncompatibleBrowser";
import { t } from "lib/localization/localization";
import "styles/reset.css";
import "styles/globals.css";

// Custom App: https://nextjs.org/docs/advanced-features/custom-app
// - The `Component` prop is the active page, so whenever you navigate between routes, `Component` will change to the new page. Therefore, any props you send to `Component` will be received by the page;
// - `pageProps` is an object with the initial props that were preloaded for your page by one of our data fetching methods, otherwise it's an empty object.
function TUIMP({ Component, pageProps }) {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale, asPath } = router;

  // Let's set rtl true if the locale is a right to left written language:
  var rtl = false;
  if (locale == "ar") {
    rtl = true;
  }

  return (
    <>
      <NextSeo
        description={t(locale, "pages-home", "pages-home-description")}
        additionalLinkTags={[
          {
            rel: "icon",
            href: "https://www.tuimp.org/favicon.ico",
          },
        ]}
        openGraph={{
          type: "website",
          url: "https://www.tuimp.org/" + locale + asPath,
          images: [
            {
              url: "https://www.tuimp.org/img/tuimp-open-graph.jpg",
              width: 1200,
              height: 630,
              alt: "TUIMP",
              type: "image/jpeg",
            },
          ],
        }}
        twitter={{
          cardType: "summary_large_image",
        }}
      />

      <IncompatibleBrowser locale={locale} />

      <Masthead locale={locale} />

      <Component {...pageProps} rtl={rtl} />

      <Script src="/js/modernizr-custom.js" />
      <Script
        data-goatcounter="https://tuimp.goatcounter.com/count"
        async
        src="//gc.zgo.at/count.js"
      />
    </>
  );
}

export default TUIMP;
