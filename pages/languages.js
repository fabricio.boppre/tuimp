import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import Link from "next/link";
import locales from "lib/localization/locales.json";

export default function Languages() {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale } = router;

  // Let's sort the locales and create the list:
  const sortedLocales = locales.sort(function (a, b) {
    a = a.language_name.toLowerCase();
    b = b.language_name.toLowerCase();
    return a < b ? -1 : a > b ? 1 : 0;
  });
  const localesList = locales.map((localeMapped, key) => (
    <li key={key}>
      <Link href="/" locale={localeMapped.language_id}>
        <a>{localeMapped.language_name}</a>
      </Link>
    </li>
  ));

  return (
    <>
      <main className="content">
        <NextSeo
          title={
            "TUIMP | " + t(locale, "pages-languages", "pages-languages-title")
          }
        />

        <h1>{t(locale, "pages-languages", "pages-languages-title")}:</h1>

        <ul>{localesList}</ul>
      </main>
    </>
  );
}
