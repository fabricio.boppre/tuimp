import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import styles from "styles/ErrorPage.module.css";

export default function Custom404() {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale } = router;

  return (
    <>
      <main id={styles.error_page} className="content">
        <NextSeo title="TUIMP" noindex={true} nofollow={true} />

        <p>{t(locale, "page-not-found", "page-not-found-message")}</p>
      </main>
    </>
  );
}
