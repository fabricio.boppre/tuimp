import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import styles from "styles/ErrorPage.module.css";

export default function Error({ statusCode }) {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale } = router;

  return (
    <>
      <main id={styles.error_page} className="content">
        <NextSeo title="TUIMP" noindex={true} nofollow={true} />

        <p>
          {statusCode
            ? `An error ${statusCode} occurred on server`
            : t(locale, "browser-error", "browser-error-message")}
        </p>
      </main>
    </>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};
