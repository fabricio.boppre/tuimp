import React, { useState } from "react";
import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { getBooklets } from "lib/booklets";
import { t } from "lib/localization/localization";
import Link from "next/link";
import styles from "styles/Booklets.module.css";

export default function Booklets({ bookletsData, rtl }) {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale } = router;

  // These constants and state controls the number of booklets being shown:
  const bookletsPerPage = 10;
  const totalBooklets = bookletsData.length;
  const [bookletsCount, setBookletsCount] = useState(bookletsPerPage);

  // Create the list of booklets according to the current bookletsCount:
  const booklets = bookletsData.slice(0, bookletsCount).map((booklet) => {
    const imageFilename =
      "/img/content/booklets/tuimp-" +
      locale +
      ("000" + booklet.number).slice(-3) +
      "-front.jpg";
    return (
      <Link
        href={`/booklet/${encodeURIComponent(booklet.number)}`}
        key={booklet.number}
      >
        <a>
          <div className="booklet">
            <div className="image">
              <img
                src={imageFilename}
                alt={booklet.title}
                layout="fixed"
                width={100}
                height={145}
              />
            </div>
            <div className="text">
              <h2>
                {booklet.number} - {booklet.title}
              </h2>
              <span>{booklet.year}</span>
            </div>
          </div>
        </a>
      </Link>
    );
  });

  // If not all the booklets are being shown, we show the button to load the next batch:
  // - The button updates the state bookletsCount and this automatically triggers a re-render, which will then show the updated booklets list.
  var loadMoreButton;
  if (totalBooklets > bookletsCount) {
    loadMoreButton = (
      <button
        onClick={() => setBookletsCount(bookletsCount + bookletsPerPage)}
        id="load-more-buton"
        className="button"
      >
        {t(locale, "booklets-index", "booklets-index-load-more")}
      </button>
    );
  } else {
    loadMoreButton = "";
  }

  return (
    <>
      <main className={"content " + (rtl ? "rtl" : "ltr")} id={styles.booklets}>
        <NextSeo
          title={
            "TUIMP | " + t(locale, "booklets-index", "booklets-index-title")
          }
        />

        <h1>{t(locale, "booklets-index", "booklets-index-title")}:</h1>

        <p>{t(locale, "booklets-index", "booklets-index-desc")}</p>

        <div id="booklets-index">{booklets}</div>

        {loadMoreButton}
      </main>
    </>
  );
}

export async function getStaticProps(context) {
  const bookletsData = await getBooklets(context.locale);
  return {
    props: {
      bookletsData,
    },
  };
}
