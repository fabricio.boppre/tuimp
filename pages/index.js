import { useRouter } from "next/router";
import { useEffect } from "react";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import styles from "styles/Home.module.css";

export default function Home({ rtl }) {
  // Let's get the active locale and all the locales from the router object:
  const router = useRouter();
  const { locale, locales } = router;

  // This effect changes the title every 4 seconds:
  useEffect(() => {
    // Let's create an array with the title in all languages:
    const titles = locales.map((locale) => {
      return t(locale, "pages-home", "pages-home-site-title");
    });
    var i = 0;
    // Now we set the title element with the first one in the array...
    document.getElementById("title").innerHTML = titles[i];
    // ... and start the interval to change it, incrementing the array index on every iteration:
    const intervalId = setInterval(() => {
      i++;
      document.getElementById("title").innerHTML = titles[i];
      // When we reach the last index, we set it -1 so on the next iteration it starts on the first index (0):
      if (i == titles.length - 1) {
        i = -1;
      }
    }, 4000);
    // Clear the setInterval on navigation:
    return () => clearInterval(intervalId);
  }, []);

  return (
    <main id={styles.home}>
      <NextSeo title="TUIMP" />

      <div id="title"></div>

      <div id="desc_and_objective">
        <div id="description">
          {t(locale, "pages-home", "pages-home-description")}
        </div>
        <div id="objective">
          {t(locale, "pages-home", "pages-home-objective")}
        </div>
      </div>
    </main>
  );
}
