import { useRouter } from "next/router";
import { getBooklet, getBookletsPaths } from "lib/booklets";
import { NextSeo } from "next-seo";
import Link from "next/link";
import { t } from "lib/localization/localization";
import styles from "styles/Booklet.module.css";

export default function Booklet({ bookletData, rtl }) {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale } = router;

  // Preparing the booklet info:
  const imageFilename =
    "/img/content/booklets/tuimp-" +
    locale +
    ("000" + bookletData.number).slice(-3) +
    "-front.jpg";
  const downloadA4URI =
    "https://minerva.ufsc.br/~natalia/tuimp/booklets/tuimp-" +
    locale +
    ("000" + bookletData.number).slice(-3) +
    "_booklet_A4.pdf";
  const downloadLetterURI =
    "https://minerva.ufsc.br/~natalia/tuimp/booklets/tuimp-" +
    locale +
    ("000" + bookletData.number).slice(-3) +
    "_booklet_letter.pdf";
  const downloadOnLineReadingURI =
    "https://minerva.ufsc.br/~natalia/tuimp/booklets/tuimp-" +
    locale +
    ("000" + bookletData.number).slice(-3) +
    "_booklet_screen.pdf";
  const downloadOnLineSmallReadingURI =
    "https://minerva.ufsc.br/~natalia/tuimp/booklets/tuimp-" +
    locale +
    ("000" + bookletData.number).slice(-3) +
    "_booklet_smallscreen.pdf";
  const authorsList = bookletData.authors.map(function (author) {
    var authorName;
    if (author.slug) {
      authorName = (
        <Link href={`/person/${encodeURIComponent(author.slug)}`}>
          <a>{author.name}</a>
        </Link>
      );
    } else {
      authorName = author.name;
    }
    return <li key={authorName}>{authorName}</li>;
  });
  var levelImage;
  var levelTitle;
  switch (bookletData.level) {
    case 1:
      levelImage = "level-1.jpg";
      levelTitle = "Easy";
      break;
    case 2:
      levelImage = "level-2.jpg";
      levelTitle = "Medium";
      break;
    case 3:
      levelImage = "level-3.jpg";
      levelTitle = "Hard";
      break;
  }

  return (
    <>
      <main className={"content " + (rtl ? "rtl" : "ltr")} id={styles.booklet}>
        <NextSeo
          title={"TUIMP | " + bookletData.number + " - " + bookletData.title}
        />

        <h1>
          {t(locale, "booklets-view", "booklets-view-title")}:{" "}
          {bookletData.number} - {bookletData.title}
        </h1>

        <div id="booklet-info">
          <div id="booklet-image">
            <img
              src={imageFilename}
              alt={bookletData.title}
              layout="fixed"
              width={200}
              height={290}
            />

            <div id="booklet-year">{bookletData.year}</div>
            <div id="booklet-level">
              {" "}
              <img
                src={"/img/layout/" + levelImage}
                alt={levelTitle}
                title={levelTitle}
                width={180}
                height={60}
              />
            </div>
          </div>

          <div id="booklet-text">
            <p>{t(locale, "booklets-view", "booklets-view-buttons-head")}:</p>

            <div>
              <a
                href={downloadA4URI}
                target="_blank"
                rel="noreferrer"
                className="booklet-download"
              >
                {t(locale, "booklets-view", "booklets-view-download-a4")}
              </a>
            </div>

            <div>
              <a
                href={downloadLetterURI}
                target="_blank"
                rel="noreferrer"
                className="booklet-download"
              >
                {t(locale, "booklets-view", "booklets-view-download-letter")}
              </a>
            </div>

            <div>
              <a
                href={downloadOnLineReadingURI}
                target="_blank"
                rel="noreferrer"
                className="booklet-download"
              >
                {t(locale, "booklets-view", "booklets-view-on-line-reading")}
              </a>
            </div>

            <div>
              <a
                href={downloadOnLineSmallReadingURI}
                target="_blank"
                rel="noreferrer"
                className="booklet-download"
              >
                {t(
                  locale,
                  "booklets-view",
                  "booklets-view-on-line-small-reading"
                )}
              </a>
            </div>

            <h2>{t(locale, "booklets-view", "booklets-view-authors")}:</h2>

            <ul>{authorsList}</ul>
          </div>

          <div id="booklet-instructions">
            <p>{t(locale, "booklets-view", "booklets-view-video")}:</p>

            <a
              href="https://www.youtube.com/watch?v=8fD2uRa9p7o"
              target="_blank"
              rel="noreferrer"
            >
              <img
                src="/img/layout/booklets-video.jpg"
                alt={t(locale, "booklets-view", "booklets-view-video")}
                layout="fixed"
                width={100}
                height={53}
              />
            </a>
          </div>
        </div>
      </main>
    </>
  );
}

// Define a list of paths (the booklets) that have to be rendered to HTML at build time:
// - If a page has dynamic routes and uses getStaticProps it needs to define this list of paths.
// - "fallback: false" means any paths not returned by getStaticPaths will result in a 404 page.
export async function getStaticPaths() {
  const paths = getBookletsPaths();
  return {
    paths: paths,
    fallback: false,
  };
}

// This function gets called at build time on server-side and fetches the post data using the library function getBooklet, which gets data from the file system:
export async function getStaticProps(context) {
  const bookletData = await getBooklet(context.locale, context.params.number);
  return {
    props: {
      bookletData,
    },
  };
}
