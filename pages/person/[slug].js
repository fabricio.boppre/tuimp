import { getPerson, getPeoplePaths } from "lib/people";
import { NextSeo } from "next-seo";
import styles from "styles/Person.module.css";

export default function Person({ personData, rtl }) {
  // Let's create the img div, if it exists:
  var personImage;
  if (personData.image !== null) {
    personImage = (
      <div id="person-image">
        <img src={"/" + personData.image} alt={personData.name} />
      </div>
    );
  }

  // Let's create the text div:
  var personText = null;
  if (personData.content !== null && personData.institution !== null) {
    if (personData.institution.name) {
      personText = (
        <div
          id="person-text"
          dangerouslySetInnerHTML={{
            __html:
              "<p>" +
              personData.institution.name +
              ", " +
              personData.institution.country +
              "</p>" +
              personData.content,
          }}
        />
      );
    } else {
      personText = (
        <div
          id="person-text"
          dangerouslySetInnerHTML={{
            __html:
              "<p>" +
              personData.institution.country +
              "</p>" +
              personData.content,
          }}
        />
      );
    }
  } else if (personData.content !== null && personData.institution == null) {
    personText = (
      <div
        id="person-text"
        dangerouslySetInnerHTML={{ __html: personData.content }}
      />
    );
  } else if (personData.content == null && personData.institution !== null) {
    if (personData.institution.name) {
      personText = (
        <div
          id="person-text"
          dangerouslySetInnerHTML={{
            __html:
              "<p>" +
              personData.institution.name +
              ", " +
              personData.institution.country +
              "</p>",
          }}
        />
      );
    } else {
      personText = (
        <div
          id="person-text"
          dangerouslySetInnerHTML={{
            __html: "<p>" + personData.institution.country + "</p>",
          }}
        />
      );
    }
  }

  return (
    <>
      <main className={"content " + (rtl ? "rtl" : "ltr")} id={styles.person}>
        <NextSeo title={"TUIMP | " + personData.name} />

        <h1>{personData.name}</h1>

        <div id="person-info">
          {personImage}

          {personText}
        </div>
      </main>
    </>
  );
}

// Define a list of paths (the people) that have to be rendered to HTML at build time:
// - If a page has dynamic routes and uses getStaticProps it needs to define this list of paths.
// - "fallback: false" means any paths not returned by getStaticPaths will result in a 404 page.
export async function getStaticPaths() {
  const paths = getPeoplePaths();
  return {
    paths: paths,
    fallback: false,
  };
}

// This function gets called at build time on server-side and fetches the person data using the library function getPerson, which gets data from the file system:
export async function getStaticProps(context) {
  const personData = await getPerson(context.locale, context.params.slug);
  return {
    props: {
      personData: personData,
    },
  };
}
