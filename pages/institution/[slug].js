import { getInstitution, getInstitutionsPaths } from "lib/institutions";
import { NextSeo } from "next-seo";
import styles from "styles/Institution.module.css";

export default function Institution({ institutionData, rtl }) {
  // Let's create the img div, if it exists:
  var institutionImage;
  if (institutionData.image !== null) {
    institutionImage = (
      <div id="institution-image">
        <img src={"/" + institutionData.image} alt={institutionData.name} />
      </div>
    );
  }

  // Let's create the text div, if it exists:
  var institutionText;
  if (institutionData.content !== null) {
    institutionText = (
      <div
        id="person-text"
        dangerouslySetInnerHTML={{ __html: institutionData.content }}
      />
    );
  } else {
    institutionText = (
      <div id="person-text">
        <img
          src={"/img/layout/under-construction.png"}
          alt={institutionData.name}
          width="70px"
          height="66px"
        />
      </div>
    );
  }

  return (
    <>
      <main
        className={"content " + (rtl ? "rtl" : "ltr")}
        id={styles.institution}
      >
        <NextSeo title={"TUIMP | " + institutionData.name} />

        <h1>{institutionData.name}</h1>

        <div id="institution-info">
          {institutionImage}

          {institutionText}
        </div>
      </main>
    </>
  );
}

// Define a list of paths (the institutions) that have to be rendered to HTML at build time:
// - If a page has dynamic routes and uses getStaticProps it needs to define this list of paths.
// - "fallback: false" means any paths not returned by getStaticPaths will result in a 404 page.
export async function getStaticPaths() {
  const paths = getInstitutionsPaths();
  return {
    paths: paths,
    fallback: false,
  };
}

// This function gets called at build time on server-side and fetches the institution data using the library function getInstitution, which gets data from the file system:
export async function getStaticProps(context) {
  const institutionData = await getInstitution(
    context.locale,
    context.params.slug
  );
  return {
    props: {
      institutionData: institutionData,
    },
  };
}
