import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { t } from "lib/localization/localization";
import styles from "styles/MessageReceived.module.css";

export default function MessageReceived() {
  // Let's get the active locale from the router object:
  const router = useRouter();
  const { locale } = router;

  return (
    <>
      <main id={styles.message_received} className="content">
        <NextSeo title="TUIMP" noindex={true} nofollow={true} />

        <p>{t(locale, "pages-about", "pages-about-form-success")}</p>
      </main>
    </>
  );
}
