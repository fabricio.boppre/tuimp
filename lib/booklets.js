import fs from "fs";
import path from "path";
import matter from "gray-matter";

// Let's create an array with all the locales folders inside /content:
const { readdirSync } = require("fs");
const localesFolders = readdirSync("content", { withFileTypes: true })
  .filter((folder) => folder.isDirectory())
  .map((folder) => folder.name);

// Function that provides the paths for the booklets:
// - It's a list with all the combinations of booklets numbers and their locales, which will be used by the Dynamic Routes logic on booklets getStaticPaths (on /booklet/[number].js)
export function getBookletsPaths() {
  var allPaths = [];
  // Let's iterate through the locales folders:
  const bookletsPaths = localesFolders.map((locale) => {
    // Get the booklets folder within the locale:
    var bookletFolder = path.join("content", locale, "booklets");
    // Get the files (the booklets) within that folder:
    var files = fs.readdirSync(bookletFolder);
    // Filter to include only * .md files (and avoid files like .DS_Stores):
    files = files.filter((file) => file.includes(".md"));
    // Create the path for each booklet (excluding the .md from the filename) of this locale:
    var thisLocalePaths = files.map((file) => ({
      params: { number: file.replace(".md", "") },
      locale: locale,
    }));
    // Add the paths to the array:
    allPaths.push(thisLocalePaths);
  });
  // Flat the paths array:
  var flattenPaths = [].concat.apply([], allPaths);
  return flattenPaths;
}

// Function that provides a booklet data:
export async function getBooklet(locale, number) {
  const bookletPath = path.join("content", locale, "booklets", `${number}.md`);
  const fileContent = fs.readFileSync(bookletPath, "utf8");
  // Use gray-matter to extract the booklets data (the YAML meta data) from the file:
  // - https://www.npmjs.com/package/gray-matter
  // - https://yaml.org/
  const matterResult = matter(fileContent, { excerpt: false });
  const booklet = {
    number: number,
    title: matterResult.data.title,
    level: matterResult.data.level,
    year: matterResult.data.year,
    authors: matterResult.data.authors,
  };
  return booklet;
}

// Function that provides the booklets index data:
export async function getBooklets(locale) {
  // Let's iterate through the files to create the booklets list:
  // - https://nodejs.org/docs/latest/api/path.html
  const bookletsPath = path.join("content", locale, "booklets");
  var files = fs.readdirSync(bookletsPath);
  // - Filter to include only * .md files (and avoid files like .DS_Stores):
  files = files.filter((file) => file.includes(".md"));
  var booklets = [];
  files.forEach(async (file) => {
    // Let's get the booklet number based on the filenames:
    const number = file.replace(".md", "");

    // Get the content from the booklet file:
    const bookletPath = path.join("content", locale, "booklets", file);
    const fileContent = fs.readFileSync(bookletPath, "utf8");

    // Use gray-matter to extract the booklets data (the YAML meta data) from the file:
    const matterResult = matter(fileContent, { excerpt: false });

    // Add the booklet to the array:
    booklets.push({
      number: number,
      title: matterResult.data.title,
      year: matterResult.data.year,
      authors: matterResult.data.authors,
    });
  });
  // Sort the booklets by number:
  booklets.sort((a, b) => {
    if (parseInt(a.number) < parseInt(b.number)) {
      return 1;
    } else {
      return -1;
    }
  });

  return booklets;
}
