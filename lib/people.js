import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { checkFileExists } from "./files";
import { remark } from "remark";
import html from "remark-html";

// Let's create an array with all the locales folders inside /content:
const { readdirSync } = require("fs");
const localesFolders = readdirSync("content", { withFileTypes: true })
  .filter((folder) => folder.isDirectory())
  .map((folder) => folder.name);

// Function that provides the paths for the people:
// - It's a list with all the combinations of people slugs and their locales, which will be used by the Dynamic Routes logic on people getStaticPaths (on /person/[slug].js)
export function getPeoplePaths() {
  var allPaths = [];
  // Let's iterate through the locales folders:
  localesFolders.map((locale) => {
    // Get the people folder within the locale:
    var peopleFolder = path.join("content", locale, "people");
    // Get the files (the people) within that folder:
    var files = fs.readdirSync(peopleFolder);
    // Filter to include only * .md files (and avoid files like .DS_Stores):
    files = files.filter((file) => file.includes(".md"));
    // Create the path for each person (excluding the .md from the filename) of this locale:
    var thisLocalePaths = files.map((file) => ({
      params: { slug: file.replace(".md", "") },
      locale: locale,
    }));
    // Add the paths to the array:
    allPaths.push(thisLocalePaths);
  });
  // Flat the paths array:
  var flattenPaths = [].concat.apply([], allPaths);
  return flattenPaths;
}

// Function that provides a person data:
export async function getPerson(locale, slug) {
  // Let's create the path for the person file...
  const personPath = path.join("content", locale, "people", `${slug}.md`);
  // ... and get its content:
  const fileContent = fs.readFileSync(personPath, "utf8");
  // Use gray-matter to extract the person data (the YAML meta data) from the file:
  // - https://www.npmjs.com/package/gray-matter
  // - https://yaml.org/
  const { content, data } = matter(fileContent, { excerpt: false });
  // Use remark to convert Markdown into HTML string:
  const processedContent = await remark().use(html).process(content);
  // Let's create the path for the person image:
  const personImagePath = path.join(
    "public",
    "img",
    "content",
    "people",
    `${slug}.jpg`
  );
  // We create the person object and return it:
  const person = {
    slug: slug,
    name: data.name,
    institution: data.institution ? data.institution : null,
    image: (await checkFileExists(personImagePath))
      ? path.join("img", "content", "people", `${slug}.jpg`)
      : null,
    content: content ? processedContent.toString() : null,
  };
  return person;
}
