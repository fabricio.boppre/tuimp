import localizationLibrary from "./localizationLibrary.json";

export function t(language_id, place, title) {
  let text = localizationLibrary.find(
    (item) =>
      item.language_id === language_id &&
      item.place === place &&
      item.title === title
  );
  return text.value;
}
