import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { checkFileExists } from "./files";
import { remark } from "remark";
import html from "remark-html";

// Let's create an array with all the locales folders inside /content:
const { readdirSync } = require("fs");
const localesFolders = readdirSync("content", { withFileTypes: true })
  .filter((folder) => folder.isDirectory())
  .map((folder) => folder.name);

// Function that provides the paths for the institutions:
// - It's a list with all the combinations of institutions slugs and their locales, which will be used by the Dynamic Routes logic on institution getStaticPaths (on /institution/[slug].js)
export function getInstitutionsPaths() {
  var allPaths = [];
  // Let's iterate through the locales folders:
  localesFolders.map((locale) => {
    // Get the people folder within the locale:
    var institutionsFolder = path.join("content", locale, "institutions");
    // Get the files (the institutions) within that folder:
    var files = fs.readdirSync(institutionsFolder);
    // Filter to include only * .md files (and avoid files like .DS_Stores):
    files = files.filter((file) => file.includes(".md"));
    // Create the path for each institution (excluding the .md from the filename) of this locale:
    var thisLocalePaths = files.map((file) => ({
      params: { slug: file.replace(".md", "") },
      locale: locale,
    }));
    // Add the paths to the array:
    allPaths.push(thisLocalePaths);
  });
  // Flat the paths array:
  var flattenPaths = [].concat.apply([], allPaths);
  return flattenPaths;
}

// Function that provides a institution data:
export async function getInstitution(locale, slug) {
  // Let's create the path for the institution file...
  const institutionPath = path.join(
    "content",
    locale,
    "institutions",
    `${slug}.md`
  );
  // ... and get its content:
  const fileContent = fs.readFileSync(institutionPath, "utf8");
  // Use gray-matter to extract the institution data (the YAML meta data) from the file:
  // - https://www.npmjs.com/package/gray-matter
  // - https://yaml.org/
  const { content, data } = matter(fileContent, { excerpt: false });
  // Use remark to convert Markdown into HTML string:
  const processedContent = await remark().use(html).process(content);
  // Let's create the path for the institution image:
  const institutionImagePath = path.join(
    "public",
    "img",
    "content",
    "institutions",
    `${slug}.jpg`
  );
  // We create the institution object and return it:
  const institution = {
    slug: slug,
    name: data.name,
    country: data.country,
    image: (await checkFileExists(institutionImagePath))
      ? path.join("img", "content", "institutions", `${slug}.jpg`)
      : null,
    content: content ? processedContent.toString() : null,
  };
  return institution;
}
