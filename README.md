# (Em português)

## Introdução

O website [TUIMP](http://tuimp.org) foi concebido pela astrônoma francesa Grażyna Stasińska e tem como objetivo divulgar conceitos de astronomia tanto para o público infantil quanto para o adulto.

O website foi construído com o framework [Next.js](https://nextjs.org) e está hospedado na [Netlify](https://www.netlify.com).

## Licenças

O código-fonte deste site está compartilhado sob a [licença MIT](LICENSE) e seu conteúdo sob a licença [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).(LICENSES.md).

# (In English)

## Introduction

The [TUIMP](http://tuimp.org) website was conceived by French astronomer Grażyna Stasińska and aims to disseminate astronomy concepts for both children and adults.

it was built with [Next.js](https://nextjs.org) and is hosted on [Netlify](https://www.netlify.com).

## Licenses

The source code of this website is shared under the [MIT license](LICENSE) and the content is under the [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.
