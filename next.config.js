const withMDX = require("@next/mdx")({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [],
    rehypePlugins: [],
  },
});

module.exports = withMDX({
  pageExtensions: ["js", "jsx", "md", "mdx"],
  reactStrictMode: true,
  i18n: {
    locales: [
      "en",
      "fr",
      "pt",
      "es",
      "pl",
      "sq",
      "it",
      "hy",
      "ru",
      "el",
      "ar",
      "om",
      "zu",
      "ro",
      "sl",
      "hi",
    ],
    defaultLocale: "en",
  },
});
