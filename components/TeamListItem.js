import Link from "next/link";

export default function TeamListItem({ item }) {
  var listItem;

  switch (item.type) {
    case "person":
      var institution = null;
      if (item.institution !== null) {
        if (item.institution.name) {
          institution =
            "(" + item.institution.name + ", " + item.institution.country + ")";
        } else {
          institution = "(" + item.institution.country + ")";
        }
      }
      listItem = (
        <li key={item.slug}>
          <Link href={`/person/${encodeURIComponent(item.slug)}`}>
            <a>{item.name}</a>
          </Link>
          &nbsp;{institution}
        </li>
      );
      break;

    case "institution":
      listItem = (
        <li key={item.slug}>
          <Link href={`/institution/${encodeURIComponent(item.slug)}`}>
            <a>{item.name}</a>
          </Link>
          &nbsp;({item.country})
        </li>
      );
      break;
  }

  return listItem;
}
