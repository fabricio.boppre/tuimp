import styles from "styles/IncompatibleBrowser.module.css";
import { t } from "lib/localization/localization";

export default function IncompatibleBrowser(props) {
  return (
    <div id={styles.incompatible_browser}>
      <p>{t(props.locale, "browser-error", "browser-error-message")}</p>
    </div>
  );
}
