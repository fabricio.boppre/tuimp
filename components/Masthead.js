import { useRouter } from "next/router";
import { useEffect } from "react";
import Link from "next/link";
import styles from "styles/Masthead.module.css";
import { t } from "lib/localization/localization";

export default function Masthead(props) {
  // Let's identifiy the place where we are to get the translations that we need here:
  const place = "menu-principal";

  // This effect changes the Masthead layout on scrolling:
  // - The opacity changes behave as a fade in/out effect because they work in sync with the transition declaration on #masthead.
  useEffect(() => {
    window.addEventListener("scroll", function (e) {
      var distanceY = window.pageYOffset || document.documentElement.scrollTop;
      const header = document.getElementById(styles.masthead);
      if (distanceY > 200) {
        if (header.classList.contains("thick")) {
          header.style.opacity = 0;
          setTimeout(function () {
            header.classList.remove("thick");
            header.classList.add("thin");
            header.style.opacity = 1;
          }, 250);
        }
      } else {
        if (header.classList.contains("thin")) {
          header.style.opacity = 0;
          setTimeout(function () {
            header.classList.remove("thin");
            header.classList.add("thick");
            header.style.opacity = 1;
          }, 250);
        }
      }
    });
  }, []);

  // If the menu is open when the route changes, we should close it:
  const router = useRouter();
  useEffect(() => {
    const mastheadOptions = document.getElementById("masthead-options");
    if (mastheadOptions.classList.contains("opened-on-tablet")) {
      toggleTabletMenu();
    }
  }, [router.asPath]);

  // Function that opens or closes the menu on tablet and bellow:
  const toggleTabletMenu = () => {
    const mastheadOptions = document.getElementById("masthead-options");
    if (mastheadOptions.classList.contains("closed-on-tablet")) {
      mastheadOptions.classList.remove("closed-on-tablet");
      mastheadOptions.classList.add("opened-on-tablet");
    } else {
      mastheadOptions.classList.remove("opened-on-tablet");
      mastheadOptions.classList.add("closed-on-tablet");
    }
  };

  return (
    <header id={styles.masthead} className="thick">
      <div id="masthead-nucleus">
        <div id="masthead-logo">
          <Link href="/">
            <a>
              <span hidden>TUIMP</span>
            </a>
          </Link>
        </div>
        <nav id="masthead-options" className="closed-on-tablet">
          <button
            id="options-button"
            onClick={() => toggleTabletMenu()}
            title={t(
              props.locale,
              place,
              "menu-principal-option-mobile-button"
            )}
          >
            <span className="label">
              {t(props.locale, place, "menu-principal-option-mobile-button")}
            </span>
          </button>

          <ul>
            <li>
              <Link href="/booklets">
                <a>
                  <img
                    src="/img/layout/header-menu-booklets.jpg"
                    alt={t(
                      props.locale,
                      place,
                      "menu-principal-option-title-booklets"
                    )}
                    width={70}
                    height={70}
                    className="image"
                  />
                  <div className="label">
                    {t(
                      props.locale,
                      place,
                      "menu-principal-option-title-booklets"
                    )}
                  </div>
                </a>
              </Link>
            </li>
            <li>
              <Link href="/about">
                <a>
                  <img
                    src="/img/layout/header-menu-about.jpg"
                    alt={t(
                      props.locale,
                      place,
                      "menu-principal-option-title-about"
                    )}
                    width={70}
                    height={70}
                    className="image"
                  />
                  <div className="label">
                    {t(
                      props.locale,
                      place,
                      "menu-principal-option-title-about"
                    )}
                  </div>
                </a>
              </Link>
            </li>
            <li>
              <Link href="/languages">
                <a>
                  <img
                    src="/img/layout/header-menu-languages.jpg"
                    alt={t(
                      props.locale,
                      place,
                      "menu-principal-option-title-languages"
                    )}
                    width={70}
                    height={70}
                    className="image"
                  />
                  <div className="label">
                    {t(
                      props.locale,
                      place,
                      "menu-principal-option-title-languages"
                    )}
                  </div>
                </a>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}
