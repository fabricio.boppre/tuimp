---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: iFlansi
---

Emva kweminyaka emkhakheni weFizikisi yezinto ezincane, uJean Schneider waphendukela kumkhakha woMkhathi, okuyiwona osebenzisa umcabango kakhulu. Ngemuva kokufunda imiThalakazi, waqhubekela phambili wayofunda ngemiZulane yamanye amalanga kanye nobhekompilo kweminye imiZulwane yezinye izinkanyezi. Waqala uhlelo lokuhlonza lemiZulwane yezinye izinkanyezi ngokusebenzisa isiZungezimhlaba i-CoRoT waphinde futhi waphakamisa indlela yokubheka imiZulwane eneziNkanyezi ezimbili. Kanti futhi uyazifela ngeFilosofi yeziLimu kanye nemibuzo ngobudlelwane phakathi koMzimba kanye neNqondo. Uthanda ukupenda ukuze achithe isizungu.
