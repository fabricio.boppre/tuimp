---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: iFlansi
---

UFrançoise Combes uyiMfundindalo yoMkhathi osebenza kwIhholobuko eParis. Eminye yemisebenzi yakhe ibandakanya ukudaleka kanye nokushintshashintsha kwemThalakazi ngokweKhozimologi ngokusemqondweni kanye nangalokho esikubonayo, sekumuwinise izindondo ezaziwa umHlaba wonke kanye nendondo embedu eyinikwa i-National Center for Scientific Research. UFrançoise ujwayele ukunikeza izifundo kanti futhi usebhale izincwadi ezibhalela abantu abangaphandle kwalomkhakha. Uyakuthanda ukuvakashela amazwe kanti futhi unempilo ecebilo ngokomndeni, nabafana bakhe abathathu kanye nabazukulu abayi-7.
