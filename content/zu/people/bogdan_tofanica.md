---
name: Bogdan Tofanica
institution:
  name: AstroClub Iași
  slug: astroclub_iasi
  country: iRumaniya
---

facebook group: [https://www.facebook.com/groups/astroclubuliasi](https://www.facebook.com/groups/astroclubuliasi)  
facebook page: [https://www.facebook.com/AstroClubul-Ia%C8%99i-107519490979284](https://www.facebook.com/AstroClubul-Ia%C8%99i-107519490979284)
