---
name: Fabricio Chiquio Boppré
---

UFabricio wazalelwa futhi uhala e-Island of Santa Catarina, edolobheni i-Florianópolis eningizimu ne-Brazil. Umhumeko wakhe umculo, kodwa futhi uyakuthanda nokuvakasha (nokuhasmba amajadu) kanye nokupheka ekhaya ntambama (ngesikhathi elalela umculo). Njengomuntu oshade imFundindalo yomkhathi, uhlale eyibuzabuza ukuthi ngabe yini eyayikhona maphambilini kokudabuka koMkhathilibe. Ngokwakhe uthi akakamuphenduli ngendlela eyanelisayo, kanti usazoqhubeka amubuze. IziNcwadi, imiDlalo kamabona kude, amafilimu asabisayo, ukubhukuda kanye nokuvakashela uLwandle kungezinye zezinto azenza nsukuzonke. Umangabe ethola iSikhathi, usebenza ukwakha iziNgosi. Ukuze wazikabanzi ngaye. cindezela [la](http://fabricioboppre.net).
