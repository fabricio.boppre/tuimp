---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: iFlansi
---

uGrażyna wazalelwa eFrance kubazali abamaPholishi kanti usehlale e-France iminyaka eminingi yempilo yakhe. UyimFundindalo yoMkhathi (Astrophysicist) kanti futhi uyilunga le-Paris Observatory. Ngokukhethekile usebenza kwimQongo kanye nemiThalakazi. Usesebenzisane kakhulu nezimFundindalo zokhathi zasemazweni i-Poland, i-Mexico,i-Brazil, kanye ne-Spain. Ezigcine sezibangabangani asondelene nabo.Ziningi izinto azilibazisa ngazo, ezibandakanya ukufunda iziLimu noma ukupheka izidlo zasemhlabeni jikelele. Kanti futhi uyathanda nokufunda iziNcwadi kanye nokulalela umculo oqhamuka kuwona wonke amagumbi amane omhlaba. Umyeni wakhe naye uyimFundindalo yoMkhathi. Banomuntwana oyedwa wesifazane, abazukulu besilisa ababili kanye namaKati amathathu.
