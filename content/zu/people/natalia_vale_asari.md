---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brazil
---

UNatalia wazalelwa ezweni lase Brazil, kwidolobha iBrasil. Uthanda ukuvakashela amazwe oMhlaba ukuze azame izinhlobonhlobo zokudla, nalolonke uhlobo lwamaPeni, imiSizi kanye namaPhepha. Uyathanda ukuba ne music record kanye neNcwadi esandleni. UMyeni wakhe uyamemangaza ngokumulethela omunye uMculo, iziNcwadi kanye namaFilimu, kanti futhi banazo neziNyoni abazigcine njengezilwane zasekhaya. Usekewahlala e-Brasil and Natal, eBrazil, e-Paris,e-France, kanye nase-Cambridge Engilande. Manje usehlala eFlorianopolis kanti futhi ufundisa iFizikisi eNyuvesi yase Federal de Santa Catarina. Ucwaningo lwakhe lugxile kwezoMkhathi yemThalakazi.
