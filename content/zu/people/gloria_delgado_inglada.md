---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: eMexico
---

[uGloria](http://www.gloriadelgadoinglada.com/) wazalelwa eMadrid, ezweni i-Spain, edolobheni alithanda kakhulu. Wathuthela eMexico ngo-2004 ukuyofunda ezoMkhathi, kusukela lapho usehlale kuleliZwe kuzekube inamuhla. Waqala ngokuhlala ePuebla kanti manje usehlala eMexico city. UCwaningo lakhe lugxile kakhulu kumaFu aluthuli ahonyozekile, uwasebenzisa ukufunda kabanzi ngokwakhiwa kanye nokushintshashintsha kwezakhi phakathi kwimThalakazi. UGloria uyathanda ukufunda kanye nokubhala, ukuvakashela amanye amazwe, ukuhamba kanye nokudlala amahora amaningi neNja yakhe uAnequi. Unendima ebalulekile ayidlala kwizinhlelo zokufinyelelisa ulwazi ngezobuchwepheshe kulabo bemiphakathi, lokhu kubandakanya amaPodcast ezobuchwepheshe, kanye nokuhlela iPhepha ndatshana i-"El Búho azul" kanye nokubhala iziqeshana mayelana nohlelo lokufinyelelisa ulwazi emiphakathini.
