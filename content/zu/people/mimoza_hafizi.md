---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: E-Albania
---

Wazalelwa eShkodra, indawo enomlando kanti futhi edolobheni elinonongonongo enyakatho ne-Albania. Wathweswa iziqu zakhe zobudokotela kwifizikisi e-Paris 7 university kanti manje usenguSolwazi wezoMkhathi eNyuvesi yaseTirana. Kwesinye isikhathi, yena kanye nabafundi babuka imiZulane, iziNkanyezi kanye nemiThalakazi ngeziPopolo zomkhathi zabo, nalapho abantu abaningi basedolobheni i-Tirana bejabulela ikuthatha iqhaza kulombukiso woMkhathi. Uthanda ukufunda iziNcwadi, ukulalela kanye nokucula uMculo, ukuhamba amabanga angama khilomitha ezihlabathini zolwandle kanye nokuvakashela amanye amazwe omhlaba jikelele nomkhwenyana wakhe. Khona lapho uphinde futhi uyatholakala nakwezombusazwe wezwe lakhe. IziNgane zakhe ezimbili zamantombazane zingabangani bakhe kanye nabeluleki. Uzinikele kwiMzulwane yamanye amaLanga kanti futhi uyilunga lethimba lezimFundindalo elizinikele ekuvubukuleni uMkhathilibe omdaladala ngama gamma ray burst, okungenye yezigemegeme ezibukhuphekhuphe kuMkhathilibe owaziwayo.
