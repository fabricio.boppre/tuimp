---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de México, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: eMexico
---

Wathweswa iziqu zakhe zobudokotela esifundweni seFizikisi eNyuvesi yaseWisconsin, nalapho waphasela ezoMkhathi kanye nemThalakazi ebonakala kwiWigo leRediyo. UnguSolwazi e-Institute of radio astronomy and astrophysics National Autonomous University yase Mexico. UCwanigo lwakhe likumkhakha wokudaleka kweziNkanyezi eziyizinguzunga kanye nomthelela walezizinkanyezi eMkhathini omaphakathi nezinkanyezi. Useqondise imibhalo enobuhlakani yokuthola iziqu eNyuvesi kusukela kumabanga aphansi efizikisi kuya emabangeni aphezulu kwizifundo zoMkhathi. Maphambilini empilweni wayengumuthwebuli weziThombe, umLimi, kanye nomumukwa neziMoto zokuzenzela. Uma enesikhathi, uyapheka, enze insimi, azigayele ubhiya, afunde, aphide afunde ezinye iziLimu, kanye nokucambalala nsukuzonke.
