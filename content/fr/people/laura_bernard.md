---
name: Laura Bernard
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Laura Bernard est née et a grandi en Lozère, une belle région de moyenne montagne dans le sud de la France. Elle est chercheuse au CNRS et travaille à l’Observatoire de Paris. Elle est spécialiste des ondes gravitationnelles et travaille sur les alternatives à la relativité générale. Elle participe à la préparation de missions futures de détection d’ondes gravitationnelles. Pendant son temps libre, elle pratique également l’athlétisme à haut niveau et participe à des courses d'orientation. Quand elle ne court pas, on pourra la retrouver plongée dans un livre, au cinéma ou en train de se ressourcer en Lozère.
