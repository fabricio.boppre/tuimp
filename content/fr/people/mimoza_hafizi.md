---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albanie
---

Mimoza est née à Shkodra, une agréable ville historique dans le nord de l’Albanie. Elle a obtenu son doctorat de l’Université de Paris 7 et est maintenant professeur d’Astrophysique à l’Université de Tirana. Parfois, avec ses étudiants, elle observe des planètes, des étoiles et des galaxies avec son propre télescope, et de nombreux habitants de Tirana les rejoignent. Elle aime lire, écouter de la musique et chanter, marcher des kilomètres au bord de la mer et voyager partout dans le monde avec son mari. Elle est aussi très active dans la vie politique de son pays. Ses deux filles sont ses meilleures amies et meilleures conseillères. Ses recherches portent sur les planètes extrasolaires et elle fait également partie d’une équipe scientifique dédiée à la découverte des premiers âges de l’Univers en utilisant les sursauts Gamma, les événements les plus énergiques de l'Univers.
