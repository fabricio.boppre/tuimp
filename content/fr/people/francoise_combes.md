---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Françoise Combes est une astrophysicienne francaise très réputée. Elle travaille à l’Observatoire de Paris. Ses nombreux travaux sur la formation et l’évolution des galaxies dans un contexte cosmologique, tant du point de vue théorique que du point de vue observationnel, lui ont valu plusieurs prix internationaux ainsi que la médaille d’or du Centre National de la Recherche Scientifique. Françoise donne souvent des conférences ouvertes à tous et a écrit plusieurs livres pour le grand public. Elle aime beaucoup les voyages. Sa vie de famille est aussi très riche, avec trois fils et sept petits-enfants.
