---
name: Julieta Fierro
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Mexique
---

Julieta Fierro travaille à l'Institut d'Astronomie de l'Université Nationale Autonome du Mexique et se consacre à la vulgarisation scientifique. Elle a travaillé dans des musées, conçu des ateliers scientifiques, écrit des livres et des articles, réalisé des séries télévisées, donné des conférences. Elle participe fréquemment à des émissions de radio et de télévision. Elle considère que la vulgarisation scientifique fait partie de l'éducation informelle, c'est-à-dire de l'éducation tout au long de la vie. Pendant son temps libre, elle aime lire et jouer de la flûte.
