---
name: Rogério Riffel
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brésil
---

Rogerio est né à Alegria, une petite ville de la campagne du Rio Grande do Sul au Brésil. Il a toujours aimé voir le ciel et essayer de le comprendre lors des longs trajets qu’il faisait vers l'école avec son frère, également astrophysicien. Il marchait 12 km par jour pour se rendre en classe. Il est titulaire d'une licence en physique, d'un master et d'un doctorat en astrophysique. Aujourd'hui, il est professeur et fait de la recherche à l'Université fédérale de Rio Grande do Sul. Ses recherches portent sur les galaxies actives et leurs trous noirs supermassifs. Il aime admirer le ciel, jouer au football, cultiver des plantes alimentaires, fabriquer de la bière et boire du maté. Il est marié et a deux enfants.
