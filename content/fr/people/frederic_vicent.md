---
name: Frédéric Vicent
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Frédéric Vicent est chercheur à l’Observatoire de Paris. Il travaille, entre autres, sur les trous noirs. Il fait partie la vaste collaboration du Event Horizon Telescope qui  a publié en 2019 la première image de l'ombre d'un trou noir.
