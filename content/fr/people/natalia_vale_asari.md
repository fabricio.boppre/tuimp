---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brésil
---

Natalia est née à Brasilia, au Brésil. Elle aime voyager pour goûter toutes sortes de plats et essayer tous les types possibles de stylos, crayons et papier. Elle aime toujours avoir un CD de musique et un livre à portée de la main. Son mari la surprend avec encore plus de musique, livres et films et ils ont quelques oiseaux libres comme animaux de compagnie. Elle a vécu à Brasilia et Natal (Brésil), à Paris (France) et à Cambridge (Angleterre). Elle vit maintenant à Florianopolis et enseigne la physique à Université Fédérale de Santa Catarina. Ses recherches portent sur l'astrophysique de galaxies.
