---
name: Georges Alecian
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Georges Alecian est astrophysicien en France. Directeur de Recherche émérite du CNRS, il travaille à l'Observatoire de Paris dans le domaine de la physique stellaire. Son activité de recherche porte essentiellement sur la composition chimique des étoiles et notamment sur les processus physiques qui créent des inhomogéneités dans la distribution des éléments. Il collabore avec des astrophysiciens de plusieurs pays, principalement la France, le Canada, l'Autriche et l'Arménie. Il aime, entre autres, la photographie, le bricolage et les gadgets high-tech. Sa femme est aussi astrophysicienne. Ils ont une fille, trois petits-fils et un nombre variable de chats.
