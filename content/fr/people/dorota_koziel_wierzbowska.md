---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Uniwersytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Pologne
---

Dorota est née à Gorlice et a passé son enfance à la campagne près de Biecz dans la Petite Pologne, d’où elle pouvait admirer sans restriction le ciel nocturne et son ruban de Voie lactée. Elle est partie à Kraków pour y étudier la physique et l'astronomie et y a vécu depuis. Elle étudie les galaxies actives, particulièrement les radiogalaxies (mais elle les étudie surtout en lumière optique). Elle continue à être émerveillée par le ciel et chaque fois que le temps, les étudiants et les comités d'allocation de temps de télescope le lui permettent, elle conduit des observations d'étoiles et de galaxies. Dans sa vie privée, elle essaie de maîtriser le doux chaos introduit récemment dans sa vie par ses deux fils jumeaux.
