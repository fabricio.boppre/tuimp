---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Grażyna est née en France de parents polonais et a vécu à Paris la plus grande partie de sa vie. Elle est astrophysicienne et travaille à l'Observatoire de Paris. Elle est spécialiste des nébuleuses et des galaxies. Elle a surtout travaillé avec des astrophysiciens polonais, mexicains, brésiliens et espagnols qui sont devenus des amis. Elle a de nombreuses passions, comme apprendre des langues ou cuisiner des plats du Monde entier. Elle aime aussi lire des livres et écouter de la musique de toutes les parties du Monde. Son mari est aussi astrophysicien. Ils ont une fille, deux petits-fils et trois chats.
