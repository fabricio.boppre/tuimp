---
name: Sylvia Ekström
institution:
  name: Observatoire de Genève
  slug: observatoire_de_geneve
  country: Suisse
---
Sylvia est née à Stockholm et a grandi à Genève. Elle a suivi une formation de sage-femme et travaillé en salle d’accouchement pendant 11 ans avant de tout quitter pour se tourner vers l’astrophysique après avoir vu la comète Hale-Bopp. Spécialiste de physique stellaire, elle est également responsable de la communication pour le Département d’astronomie de l’Université de Genève. Mariée et cohabitant avec deux chats, elle consacre son temps libre au chant et au violoncelle, et donne régulièrement des concerts.
