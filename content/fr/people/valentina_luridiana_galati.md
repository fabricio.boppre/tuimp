---
name: Valentina Luridiana Galati
institution:
  name: Instituto de Astrofisica de Canarias
  slug: instituto_de_astrofisica_de_canarias
  country: Espagne
---

Astrophysicienne et traductrice indépendante.
