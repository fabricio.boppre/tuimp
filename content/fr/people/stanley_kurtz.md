---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de México, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: Mexique
---

Stan a obtenu son doctorat en physique à l'université de Wisconsin, avec une spécialisation en astrophysique et en radioastronomie. Il est actuellement professeur à l'institut de radioastronomie et astrophysique de l'Université autonome nationale du Mexique. Ses recherches portent sur les régions de formation d'étoiles massives et sur les effets de ces étoiles sur leur environnement. Il a dirigé de nombreuses thèses depuis la licence jusqu’au doctorat et enseigne la physique et l’astronomie a l’université. Auparavant il a eté photographe, fermier et grand amateur de voitures américaines. S'il avait du temps libre, il pourrait l'utiliser pour cuisiner, jardiner, fabriquer de la bière, lire beaucoup (de préférence en sirotant de la bière), apprendre des langues et faire une petite sieste tous les jours.
