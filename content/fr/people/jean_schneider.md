---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Après avoir travaillé en microphysique, Jean Schneider s'est tourné vers l'astronomie qui met davantage en jeu l'imagination. Des galaxies, il est passé aux exoplanètes et à la recherche de la vie autour d'autres étoiles. Il a initié la détection d'exoplanètes avec le satellite CoRoT et proposé une méthode de recherche de planètes avec deux soleils. Parallèlement, il s'intéresse à la philosophie du langage et à la question des rapports entre le corps et l’esprit. Récemment il s’est aussi mis à l’aquarelle.
