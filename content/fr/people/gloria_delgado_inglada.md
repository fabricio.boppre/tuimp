---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Mexique
---

[Gloria](http://www.gloriadelgadoinglada.com/) est née à Madrid, en Espagne, une ville qu'elle adore. Elle est partie au Mexique en 2004 pour y étudier l'astronomie et a vécu dans ce pays depuis lors, d'abord à Puebla et maintenant à Mexico. Ses recherches portent principalement sur les nébuleuses ionisées. Grâce à elles elle peut étudier la production des éléments chimiques et leur évolution dans les galaxies. Gloria aime lire et écrire, voyager, marcher et jouer pendant des heures avec son chien [Anequi](http://www.gloriadelgadoinglada.com/anequi). Elle consacre beaucoup de temps à la diffusion des sciences, participe à des [podcasts](http://aficionporlaciencia.blogspot.mx/), édite le bulletin ["El Búho azul"](http://sam.org.mx/editorial-sam/el-buho-azul.php) (la Chouette bleue) et écrit des articles de vulgarisation scientifique.
