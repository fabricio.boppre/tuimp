---
name: Fabricio Chiquio Boppré
---

Fabricio est né et a toujours vécu à Florianópolis sur l'île de Santa Catarina, au sud du Brésil. Sa passion principale est la musique, mais il aime aussi voyager (pour assister à des concerts) et, le soir, cuisiner chez lui (en écoutant sa collection de CDs). Marié à une astrophysicienne, il ne cesse de demander à son epouse ce qu’il y a eu avant le Big Bang. Estimant qu'elle ne lui a pas encore répondu de manière satisfaisante il continuera à le lui demander. La littérature, les jeux vidéo, les films d'horreur, la natation et les promenades sur la plage comptent parmi ses autres occupations. Quand il a du temps, il développe des sites Internet. Pour en savoir plus sur lui, clique [ici](http://www.fabricioboppre.net/).
