---
name: Alexandre Le Tiec
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Chercheur au CNRS, Alexandre travaille à l'Observatoire de Paris, dans le domaine de l'astrophysique des hautes énergies. Son activité de recherche porte essentiellement sur la physique des trous noirs et sur les ondes gravitationnelles générées par les couples d'astres compacts. Récemment, il s'est pris de passion pour l'histoire de la vie sur Terre et celle de notre espèce, Homo sapiens. En parallèle de ses recherches en astrophysique, il mène désormais une enquête à travers les sciences naturelles, humaines et sociales, visant à remonter jusqu'aux racines de l'Anthropocène. À ses heures perdues, il bouquine, écoute de la musique, et se balade à pieds, parfois en même temps.
