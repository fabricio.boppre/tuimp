---
name: Marina Trevisan
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brésil
---

Marina est née à Curitiba, mais a grandi à Itajaí et a vécu dans plusieurs autres villes du Brésil. Elle a également vécu à Londres (Angleterre) et à Paris (France). Aujourd'hui, elle est à Porto Alegre et elle est professeure et chercheuse à l'Université fédérale de Rio Grande do Sul. Ses recherches portent sur la formation et l'évolution des galaxies. Elle est passionnée d'astronomie depuis son enfance et perd encore le souffle lorsqu'elle voit un ciel sombre et étoilé. Elle est connue par ses amis pour être une passionnée des chats, mais elle n'en a que quatre par manque de place. Elle est mariée et a une fille.
