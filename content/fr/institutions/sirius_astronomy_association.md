---
name: Association d'astronomie Sirius
country: Algérie
---

**Traducteurs:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Supervision scientifique:**  
Jamal Mimouni  
Hichem Guergouri

**Supervision linguistique:**  
Hafsa Bourkab
