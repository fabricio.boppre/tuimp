---
name: Associazione di astronomia Sirius
country: Algeria
---

**Traduttori:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Supervisione scientifica:**  
Jamal Mimouni  
Hichem Guergouri

**Supervisione linguistica:**  
Hafsa Bourkab
