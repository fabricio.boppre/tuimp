---
name: Jimma Università
country: Etiopia
---

Abdisa Tesema - MSc. in Astrofisica  
Bikila Teshome - MSc. in Astrofisica  
Jifar Raya - MSc. in Astrofisica  
Sena Bokona - MSc. in Astrofisica  
Tolu Biressa - PhD in Astrofisica
