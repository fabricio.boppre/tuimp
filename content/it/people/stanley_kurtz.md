---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de México, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: Messico
---

Stan ha ottenuto il suo dottorato in fisica nell’Università di Wisconsin, specializzandosi in astrofisica e radioastronomia. Attualmente è professore a tempo completo nell’Istituto di Radioastronomia e Astrofisica dell’Università Nazionale Autonoma del Messico. La sua ricerca si basa sulla formazione di stelle massicce e sugli effetti che queste stelle producono sul mezzo interstellare. Ha diretto progetti di tesi a livello universitario per studenti della triennale e della magistrale, ai quali da anche lezioni di fisica e astronomia. In passato fu fotografo, agricoltore e fanatico dei "muscle cars". Se avesse del tempo libero, gli piacerebbe cucinare, occuparsi del giardino, produrre birra artigianale, leggere tanto (possibilmente mentre beve la birra), imparare altre lingue e fare una pennichella ogni giorno.
