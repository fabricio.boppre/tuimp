---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albania
---

Mimoza è nata a Shkodra, una città storica molto bella al nord dell’Albania. Ha ottenuto un dottorato in fisica nell’Università di Parigi VII ed è attualmente professoressa di astrofisica nell’Università di Tirana. A volte, con i suoi alunni realizza osservazioni di pianeti, stelle e galassie con il suo telescopio personale, alle quali partecipano anche molte persone della città di Tirana. Le piace leggere, ascoltare musica, cantare, camminare chilometri e chilometri sulla riva del mare e viaggiare in tutto il mondo con suo marito. È molto attiva a livello politico nel suo paese. Le sue due figlie sono le sue migliori amiche e consigliere. Per quanto riguarda le sue ricerche, Mimoza studia i pianeti extrasolari e fa parte di un gruppo che si dedica a scoprire l’Universo primitivo attraverso esplosioni di raggi gamma, gli eventi più energetici dell’universo.
