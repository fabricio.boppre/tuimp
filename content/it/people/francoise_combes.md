---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francia
---

Françoise Combes è un'astrofisica francese molto famosa. Lavora all'Osservatorio di Parigi. I suoi numerosi lavori sulla formazione e l'evoluzione delle galassie in un contesto cosmologico, sia dal punto di vista teorico che osservativo, le sono valsi numerosi premi internazionali oltre alla medaglia d'oro del Centro Nazionale per la Ricerca Scientifica. Françoise tiene spesso conferenze aperte a tutti e ha scritto diversi libri per il grande pubblico. Le piace molto viaggiare. Anche la sua vita familiare è molto ricca, con tre figli e sette nipoti.
