---
name: Fabricio Chiquio Boppré
---

Fabricio è nato e vive tuttora nell’isola di Santa Catarina, nella città di Florianópolis, al sud del Brasile. La sua passione principale è la musica, però gli piace molto anche viaggiare (per andare ai concerti) e cucinare la cena a casa (mentre ascolta la sua collezione di dischi). È sposato con un’astrofisica, alla quale ha sempre chiesto cosa è successo prima del Big Bang. Dato che pensa che lei non gli abbia ancora risposto in modo soddisfacente, continua a ripeterle la stessa domanda. La letteratura, i videogiochi, i film di paura, nuotare e andare in spiaggia sono altre sue occupazioni quotidiane. Quando ha abbastanza tempo, crea pagine web. Per sapere di più sulla sua vita clicca [qui](http://www.fabricioboppre.net/).
