---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Messico
---

[Gloria](http://www.gloriadelgadoinglada.com/) è nata a Madrid, in Spagna, una città che le piace moltissimo. Si è trasferita in Messico nel 2004 per studiare astronomia. Da allora vive in Messico, ed ha abitato nella città della Puebla e successivamente nella Città del Messico, dove risiede tuttora. La sua specialità sono le nebulose ionizzate ed in particolare studia la produzione e l’evoluzione degli elementi chimici nelle galassie. Le piace leggere e scrivere, viaggiare, camminare e giocare per ore con il suo incredibile cagnolino Anequi. È molto attiva nella divulgazione, crea [podcasts](http://aficionporlaciencia.blogspot.mx/) di scienze, si occupa dell'edizione del bollettino "El Búho azul" e scrive articoli di divulgazione.
