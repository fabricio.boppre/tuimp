---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brasile
---

Natalia è nata a Brasilia, in Brasile. Adora viaggiare e provare tutti i tipi di cibo e tutti i tipi di penne, matite e fogli che trova. Ha sempre a portata di mano un disco musicale ed un libro. Suo marito la sorprende con ancora più musica, libri e film, ed hanno come animali domestici degli uccelli liberi che fanno liberamente il nido nei pressi della loro casa. Natalia ha vissuto a Brasilia e a Natal, in Brasile, a Parigi, in Francia, e a Cambridge, in Inghilterra. Adesso vive a Florianópolis e insegna fisica all’Università Federal di Santa Catarina. È specializzata nello studio delle galassie.
