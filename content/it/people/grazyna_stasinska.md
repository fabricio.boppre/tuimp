---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francia
---

Grażyna è nata in Francia da genitori polacchi e ha vissuto a Parigi la maggior parte della sua vita. È un’astrofisica ed è membro dell’Osservatorio di Parigi. Grażyna è specializzata nello studio di nebulose e galassie. Nell’arco della sua carriera ha lavorato maggiormente con astrofisici polacchi, messicani, brasiliani e spagnoli, i quali sono finiti per diventare suoi intimi amici. Ha molti hobbies, come imparare nuove lingue o cucinare piatti originari di svariate parti del mondo. Adora anche leggere libri e ascoltare musica proveniente da tutte le parti del mondo. Suo marito è anche astrofisico. Hanno una figlia, due nipotini e tre gatti.
