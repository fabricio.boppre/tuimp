---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Uniwersytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Polonia
---

Dorota è nata a Gorlice e ha passato la sua infanzia in un paesino vicino a Biecz, nel voivodato della Piccola Polonia, dove ha potuto godere senza limiti del cielo scuro con la Via Lattea ben visibile. Successivamente, si è trasferita a Cracovia per studiare fisica e astronomia e da allora ha stabilito un legame profondo con questa città e con i suoi dintorni. Dorota studia le galassie attive, principalmente le radiogalassie, anche se maggiormente nella banda ottica. È ancora affascinata dal cielo e quando il tempo, gli studenti e il comitato che regola l’assegnazione del tempo di osservazione al telescopio lo permette, realizza osservazioni di stelle e galassie. Nella vita personale cerca di mantenere il controllo del dolce caos che i suoi figli gemelli hanno portato nella sua vita.
