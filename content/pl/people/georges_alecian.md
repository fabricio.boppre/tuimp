---
name: Georges Alecian
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francja
---

Georges Alecian jest astrofizykiem z Francji. Jest profesorem emeritus CNRS i pracuje w Obserwatorium Paryskim w dziedzinie fizyki gwiazdowej. Jego działalność badawcza koncentruje się na składzie chemicznym gwiazd, a w szczególności na procesach fizycznych, które tworzą niejednorodności w dystrybucji pierwiastków. Współpracuje z astrofizykami z kilku krajów, głównie z Francji, Kanady, Austrii i Armenii. Jego hobby to fotografia, majsterkowanie i zaawansowane technologicznie gadżety. Jego żona również jest astrofizykiem. Mają córkę, trzech wnuków i zmienną liczbę kotów.
