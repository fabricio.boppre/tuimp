---
name: Marina Trevisan
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazylia
---

Marina urodziła się w Kurytybie, ale dorastała w Itajaí i mieszkała w kilku innych miastach w Brazylii. Mieszkała również w Londynie (Anglia) i Paryżu (Francja). Obecnie przebywa w Porto Alegre, gdzie jest profesorem i pracownikiem naukowym na Uniwersytecie Federalnym Rio Grande do Sul. Jej badania dotyczaą formowaniu się i ewolucji galaktyk. Od dziecka pasjonowała się astronomią i do dziś traci oddech na widok ciemnego, rozgwieżdżonego nieba. Wśród przyjaciół znana jest jako miłośniczka kotów, ale z braku miejsca ma ich tylko cztery. Jest mężatką i ma córkę.
