---
name: Fabricio Chiquio Boppré
---

Fabricio urodził się i mieszka na Wyspie Świętej Katarzyny w mieście Florianopolis na południu Brazylii. Jego główną pasją jest muzyka, ale uwielbia także podróżować (by móc uczestniczyć w koncertach) oraz gotować w domu po nocach (podczas słuchania jego ulubionego zestawu płyt). Będąc w związku małżeńskim z astrofizykiem, ciągle pyta żonę co istniało przed Wielkim Wybuchem. Ponieważ wciąż nie czuje się usatysfakcjonowany odpowiedziami, nadal pyta. Literatura, gry wideo, horrory, pływanie oraz chodzenie na plażę to pozostałe z jego codziennych zajęć. Gdy ma wystarczająco czasu pracuje rozwijając strony internetowe. Aby dowiedzieć się o nim więcej kliknij [tutaj](http://www.fabricioboppre.net/).
