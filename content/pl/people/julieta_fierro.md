---
name: Julieta Fierro
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Meksyk
---

Julieta Fierro pracuje w Instytucie Astronomii Narodowego Uniwersytetu Autonomicznego w Meksyku i zajmuje się popularyzacją nauki. Pracowała w muzeach, prowadziła warsztaty naukowe, napisała wiele książeki i artykułów, produkowała seriale telewizyjne i wygłaszała wykłady. Jest częstym uczestnikiem programów radiowych i telewizyjnych. Popularyzację nauki uważa za część edukacji nieformalnej, czyli uczenia się przez całe życie. W wolnym czasie lubi czytać i grać na flecie.
