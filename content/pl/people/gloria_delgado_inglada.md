---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Meksyk
---

[Gloria](http://www.gloriadelgadoinglada.com/) urodziła się w Madrycie w Hiszpanii, w mieście, które naprawdę kocha. W 2004 roku przeprowadziła się do Meksyku by studiować astronomię i od tej pory mieszka w tym kraju, najpierw w Puebla a teraz w mieście Meksyk. W swych badaniach skupia się na mgławicach zjonizowanych, Gloria używa tych obiektów by dowiedzieć się czegoś o produkcji i ewolucji pierwiastków w galaktykach. Gloria uwielbia czytać i pisać, podróżować, oraz spacerować i bawić się godzinami z jej wspaniałym psem Anequi. Jest ona bardzo aktywna we wszelkich działaniach popularyzatorskich, włączając w to popularne [podcasty](http://aficionporlaciencia.blogspot.mx/) o nauce, edycję biuletynu "El Búho azul", oraz pisanie artykułów popularno-naukowych.
