---
name: Rogério Riffel
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazylia
---

Rogerio urodził się w Alegria, małym miasteczku na wsi w stanie Rio Grande do Sul w Brazylii. Zawsze lubił obserwować niebo i próbować je zrozumieć podczas porannych wędrowań do szkoły z bratem, który również jest astrofizykiem. Zwykle pokonywał 12 km dziennie, aby dotrzeć na zajęcia. Ma dyplom z fizyki, oraz magistra i doktora z astrofizyki. Obecnie jest profesorem i prowadzi badania naukowe na Uniwersytecie Federalnym Rio Grande do Sul. Jego badania związane są z aktywnymi galaktykami i ich supermasywnymi czarnymi dziurami. Lubi ogląać niebo, grać w piłkę nożną, uprawiać rośliny jadalne, warzyć piwo i pić mate. Jest żonaty i ma dwoje dzieci.
