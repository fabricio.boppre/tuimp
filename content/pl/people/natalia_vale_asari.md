---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brazylia
---

Natalia urodziła się w Brasilia, stolicy Brazylii. Uwielbia podróżować by próbować różnych dań, oraz różnych rodzajów długopisów, ołówków i papieru. Lubi mieć stale przy sobie jakąś płytę z muzyką oraz książkę w ręce. Jej mąż zaskakuje ją coraz to nowszymi piosenkami, książkami i filmami, jako zwierzaki mają kilka wolnych, gniazdujących ptaków. Mieszkała w Brasilia i Natal (w Brazylii), w Paryżu (we Francji) oraz w Cambridge (w Anglii). Obecnie mieszka we Florianopolis oraz uczy fizyki na Uniwersytecie Federalnym Świętej Katarzyny. Dziedziną jaką zajmuje się jest astrofizyka galaktyk.
