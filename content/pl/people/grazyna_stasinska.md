---
name: Grażyna Stasińska
institution:
  name: Obserwatoire de Paris
  slug: observatoire_de_paris
  country: Francja
---

Grażyna urodziła się we Francji, w polskiej rodzinie, i większość swojego życia spędziła w Paryżu. Jest astrofizykiem i pracuje w Obserwatorium Paryskim. Specjalizuje się w badaniach mgławic i galaktyk. Pracowała głównie z polskimi, meksykańskimi, brazylijskimi i hiszpańskimi astrofizykami, którzy z czasem stali się jej bliskimi przyjaciółmi. Ma wiele zainteresowań jak uczenie się języków obcych czy gotowanie posiłków z całego świata. Uwielbia czytać książki oraz słuchać muzyki z różnych zakątków świata. Jej mąż także jest astrofizykiem. Razem mają jedną córkę, dwóch wnuków, oraz trzy koty.
