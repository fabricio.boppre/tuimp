---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de México, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: Meksyk
---

Stan uzyskał doktorat w dziedzinie fizyki na Uniwersytecie w Wisconsin, ze specjalizacją w astrofizyce i radioastronomii. Obecnie jest profesorem w Instytucie Radioastronomii i Astrofizyki Narodowego Uniwersytetu Autonomicznego Meksyku. Zajmuje się badaniem formowania się masywnych gwiazd oraz wpływu tych gwiazd na ośrodek międzygwiazdowy. Stan był promotorem prac dyplomowych od poziomu licencjata do doktora, oraz regularnie uczy studentów studiów licencjackich fizyki oraz studentów studiów magisterskich astronomii. Wcześniej był fotografem, rolnikiem, oraz entuzjastą tzw. muscle car. Gdyby miał trochę wolnego czasu mógłby go poświęcić na gotowanie, zajmowanie się ogrodem, warzenie piwa, czytanie (najlepiej przy piwie), uczenie się języków obcych oraz na drzemkę każdego dnia.
