---
name: Sylvia Ekström
institution:
  name: Observatoire de Genève
  slug: observatoire_de_geneve
  country: Szwajcaria
---
Sylvia urodziła się w Sztokholmie, a wychowała się w Genewie. Wykształciła się na położną i pracowała na sali porodowej przez 11 lat, zanim porzuciła wszystko, by zająć się astrofizyką po tym, jak zobaczyła kometę Hale-Bopp. Jest specjalistką w dziedzinie fizyki gwiezdnej, a także kierownikiem ds. komunikacji na Wydziale Astronomii Uniwersytetu Genewskiego. Mężatka z dwoma kotami, spędza wolny czas śpiewając i grając na wiolonczeli i regularnie daje koncerty.
