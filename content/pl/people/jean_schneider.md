---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francja
---

Po pracy w mikrofizyce, Jean Schneider przeszedł do pracy w astronomii, w której wyobraźnia gra większą rolę. Z galaktyk przeniósł się do egzoplanet i poszukiwania życia wokół innych gwiazd. Zainicjował program poszukiwania egzoplanet za pomocą satelity CoRoT i zaproponował metodę wykrywania planet z dwoma słońcami. Interesuje się również filozofią języka i relacją między ciałem a umysłem. W wolnej chwili maluje akwarele.
