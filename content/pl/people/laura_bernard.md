---
name: Laura Bernard
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Laura Bernard urodziła się i wychowała w Lozère, pięknym regionie śróedniogórskim na południu Francji. Jest badaczką w CNRS i pracuje w Obserwatorium Paryskim. Specjalizuje się w falach grawitacyjnych i szuka alternatyw dla ogólnej teorii względności. Bierze udział w przygotowaniach do przyszłych misji, które będą wykrywać fale grawitacyjne. W wolnym czasie uprawia lekkoatletykę i bierze udział w wyścigach długodystansowych. Kiedy nie biega, lubi czytać książki, chodzić do kina lub odpoczywać w Lozère.
