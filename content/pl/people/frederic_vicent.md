---
name: Frédéric Vicent
institution:
  name: Obserwatoire de Paris
  slug: observatoire_de_paris
  country: Francja
---

Frédéric Vicent pracuje w Obserwatorium Paryskim. Zajmuje się między innymi czarnymi dziurami. Jest członkiem ogromnej współpracy Event Horizon Telescope, która w 2019 r. opublikowała pierwszy obraz cienia czarnej dziury.
