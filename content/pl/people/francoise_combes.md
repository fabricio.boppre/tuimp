---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francja
---

Françoise Combes jest bardzo znaną francuską astrofizyk. Pracuje w Obserwatorium Paryskim. Jej liczne prace dotyczące powstawania i ewolucji galaktyk w kontekście kosmologicznym (zarówno z teoretycznego, jak i obserwacyjnego punktu widzenia) przyniosły jej wiele międzynarodowych nagród, a także złoty medal Narodowego Centrum Badań Naukowych. Françoise często prowadzi otwarte wykłady i jest autorem kilku książek dla szerokiej publiczności. Bardzo lubi podróżować. Jej życie rodzinne jest również bardzo bogate, ma trzech synów i siedmioro wnucząt.
