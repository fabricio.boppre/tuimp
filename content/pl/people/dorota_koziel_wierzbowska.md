---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Uniwersytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Polska
---

Dorota urodziła się w Gorlicach, a dzieciństwo spędziła na wsi niedaleko Biecza w Małopolsce, gdzie bez ograniczeń mogła podziwiać ciemne niebo ze wstęgą Drogi Mleczej. Przeprowadziła się do Krakowa by móc studiować fizykę i astronomię, i dalsze jej życie związane jest z tym miastem i okolicami. Zajmuje się badaniem aktywnych galaktyk, szczególnie radiogalaktyk chociaż głównie w świetle widzialnym. Nadal jest zauroczona niebem, i gdy tylko pozwalają jej na to czas, studenci oraz komisje przydziału czasu teleskopowego, prowadzi własne obserwacje gwiazd i galaktyk. W chwili obecnej próbuje opanować przyjemny chaos wprowadzony do jej życia przez dwóch synów - bliźniaków.
