---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albania
---

Mimoza urodziła się w Shkodra, historycznym i bardzo miłym mieście w północnej części Albanii. Doktorat z fizyki uzyskała na Uniwersytecie Paris 7, a obecnie jest profesorem zwyczajnym w dziedzinie astrofizyki na Uniwersytecie w Tiranie. Czasem ze swoimi uczniami prowadzi obserwacje planet, gwiazd i galaktyk za pomocą własnego teleskopu, w czym chętnie uczestniczy wielu ludzi z Tirany. Lubi czytać książki, słuchać muzyki i śpiewać, spacerować kilometrami nad morzem i podróżować po całym świecie ze swoim mężem. Jednocześnie bierze aktywny udział w życiu politycznym swojego kraju. Jej dwie córki to najlepsi przyjaciele i doradcy. Naukowo zajmuje się planetami pozasłonecznymi i równocześnie bierze udział w pracach zespołu badającego wczesny Wszechświat za pomocą rozbłysków Gamma, najbardziej energetycznych wydarzeń we Wszechświecie.
