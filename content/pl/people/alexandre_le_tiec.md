---
name: Alexandre Le Tiec
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Alexandre jest badaczem CNRS pracującym w Obserwatorium Paryskim w dziedzinie astrofizyki wysokich energii. Jego badania koncentrują się głównie na fizyce czarnych dziur i fal grawitacyjnych generowanych przez pary zwartych gwiazd. Ostatnio rozwinął pasję do historii życia na Ziemi i historii naszego gatunku, Homo sapiens. Oprócz badań z zakresu astrofizyki prowadzi obecnie badania z zakresu nauk przyrodniczych, humanistycznych i społecznych, których celem jest prześledzenie korzeni antropocenu. W wolnym czasie czyta książki, słucha muzyki i chodzi na spacery, czasami w tym samym czasie.
