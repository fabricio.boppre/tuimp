---
name: Stowarzyszenie Astronomiczne Sirius
country: Algieria
---

**Tłumacze:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Nadzór naukowy:**  
Jamal Mimouni  
Hichem Guergouri

**Nadzór lingwistyczny:**  
Hafsa Bourkab
