---
name: Сириус Астрономическая Ассоциация
country: Алжир
---

**Переводчики:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Научное руководство:**  
Jamal Mimouni  
Hichem Guergouri

**Лингвистический надзор:**  
Hafsa Bourkab
