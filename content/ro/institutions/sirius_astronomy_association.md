---
name: Asociația de Astronomie Sirius
country: Algeria
---

**Traducători:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Supravegherea științifică:**  
Jamal Mimouni  
Hichem Guergouri

**Supervizare lingvistică:**  
Hafsa Bourkab
