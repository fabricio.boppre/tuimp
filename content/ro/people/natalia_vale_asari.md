---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brazilia
---

Natalia s-a născut în orașul Brasilia din Brazilia. Îi place să călătorească pentru a încerca tot felul de alimente și toate tipurile de pixuri, creioane și hârtie. Îi place să aibă întotdeauna la îndemână muzică și o carte. Soțul ei o surprinde cu încă și mai multă muzică, cărți și filme și au câteva cuiburi de păsări libere ca animale de companie. A locuit în Brasilia și Natal, din Brazilia, la Paris, Franța și la Cambridge, Anglia. Acum locuiește în Florianopolis și predă fizică la Universitatea Federală din Santa Catarina. Cercetările ei sunt legate de astrofizica galaxiilor.
