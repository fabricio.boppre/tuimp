---
name: Stanley Kurtz
institution:
  name: Universidad Național Autonoma de Mexico, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: Mexic
---

Stan a obținut doctoratul în fizică la Universitatea din Wisconsin, cu specializarea în astrofizică și radioastronomie. În prezent este profesor titular la Institutul de Radioastronomie și Astrofizică al Universității Naționale Autonome din Mexic. Cercetările sale sunt legate de formarea stelelor masive și efectul acestor stele asupra mediului interstelar. Este conducător de teze de licență, masterat și doctorat și predă cursuri de fizică pentru licență și de astronomie pentru masterat. În tinerețe a fost fotograf, fermier și pasionat de autovehicule puternice. Dacă ar avea timp liber, ar putea să-l folosească pentru a găti, a grădinări, a prepara bere artizanală, a citi (de preferință în timp ce bea bere), a învăța alte limbi și a trage un pui de somn în fiecare zi.
