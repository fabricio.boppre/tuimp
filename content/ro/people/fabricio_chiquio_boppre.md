---
name: Fabricio Chiquio Boppré
---

Fabricio s-a născut și trăiește în orașul Florianópolis de pe Insula Santa Catarina, în sudul Braziliei. Pasiunea sa principală este muzica, dar îi place, să călătorească (să participe la concerte) și să gătească acasă seara (în timp ce ascultă colecția sa de discuri). Fiind căsătorit cu un astrofizician, el o întreabă mereu ce a existat înainte de Big Bang. Deoarece consideră că nu a primit încă un răspuns satisfăcător, va continua să întrebe. Literatura, jocurile video, filmele de groază, înotul și mersul la plajă sunt alte dintre ocupațiile sale zilnice. Când este suficient timp, lucrează dezvoltând site-uri web. Pentru a afla mai multe despre el, vizitați pe pagina lui personală [aici](http://fabricioboppre.net).
