---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Franța
---

Grażyna s-a născut în Franța din părinți polonezi și a trăit la Paris cea mai mare parte a vieții sale. Este astrofiziciană angajată a Observatorului din Paris. Este specializată în nebuloase și galaxii. A lucrat mai ales cu astrofizicieni polonezi, mexicani, brazilieni și spanioli care i-au devenit prieteni apropiați. Are multe hobby-uri, cum ar fi învățarea limbilor străine sau gătitul meselor din întreaga lume. De asemenea, îi place să citească cărți și să asculte muzică din toate părțile lumii. Soțul ei este, de asemenea, astrofizician. Au o fiică, doi nepoți și trei pisici.
