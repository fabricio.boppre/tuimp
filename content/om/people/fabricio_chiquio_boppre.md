---
name: Fabricio Chiquio Boppré
---

Faabrikoon kan dhalatee fi jiraatu, Odola Kaataarinaa Saantaa, magaalaa Filooriyaanooppilis, kibba Biraaziil keessa dha. Fedhiin isaa inni guddaan muuziqaa dha; garuu deemuu (qophii sirbaa horduuf) fi galgala (sirboota waraabbate dhaggeeffachaa) manatti nyaata bilcheessuus ni jaallata. Hayyuu Astiroofiiziksii fuudhee, yeroo hunda Biigbaangii dura maal akka ture ishee gaafata. Amma illee akka waan isheen deebii quubsaa hin deebisneefiitti waan yaaduuf, gaafachuu ittuma fufa. Hogbarruu, tapha ykn geemii viidiyoo, fiilmiiwwan sodaachisoo, bishaan daakuu fi naannoo lageenii ykn galaanaa deemuun hojiiwwan isaa guyya guyyaa biroo ti. Yoo yeroo gahaa argate, marsariitiiwwan ykn toora interneetii garaagaraa fooyyessuu irratti hojjeta.
