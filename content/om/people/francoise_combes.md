---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Faransaay
---

Firaansuwee Kombees, Astiroofiiziksii baay’ee beekamtuu Faransaay, Buufa Qorannoo Paaris keessa hojjettuu dha. Hojiiwwan isheen uumamaa fi jijjiirama suutaa gaalaaksotaa irratti akkaataa kosmooloojiin, gama tiyoorii fi daawwannaatiin hojjette, dhaabbata ‘Giddu-galeessa Biyyaa Qorannoo Saayinsawaaf’ jedhamu irraa badhaasawwan addunyaa hedduu fi madaaliyaa warqii akka isheen argattu taasisee jira. Firaansuween yeroo mara barumsa hubannoo kennuu fi kitaabilee hedduu hawaasa maraaf gumaachiti. Deemuu jaallatti; akkasumas jireenya maatii badhaadhaa, ijoollota dhiiraa sadii fi akkawoowwan torba qabdi.
