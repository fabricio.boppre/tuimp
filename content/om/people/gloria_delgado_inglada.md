---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Meeksikoo
---

[Gilooriyaan](http://www.gloriadelgadoinglada.com/) biyya Ispeen, magaalaa jaallattu Maadriiditti dhalatte.Bara 2004 Astironoomii barachuuf gara Meksiikoo deemtee, ergasii biyyuma kana jiraachaa turtee,jalqaba Pu’eeblaa amma immoo magaalaa Meksiikoo keessa jiraachaa jirti. Qorannoon ishee caalaatti nebulaawwan ayoona’an irratti xiyyeeffata; kanas waa’ee uumamuu ykn oomishamuu,fi jijjiirama suutaa elementoota gaalaaksii keessaa barachuuf itti fayyadamte. Gilooriyaan dubbisuu fi barreessuu akkasumas deemuu fi saree ishee dinqisiisaa kan ‘Aneekuwii’ jedhamu waliiin sa’a murtaa’eef taphachuu jaallatti. Sosochii hubannoo uumuu irratti, gama saayinsiin kan dhaggeeffachuun baratan (poodkaastii) dabalatee, barruu xiqqaa ‘Eel Biichoo Azul’ jedhamu gulaaluu fi barruulee adda addaa barreessuu irratti baay’ee cimtuu dha.
