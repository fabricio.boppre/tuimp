---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albaaniyaa
---

Mimoozaan kan dhalatte magaalaa seena qabeettii fi baay’ee bareedduu,kutaa kaaba Albaaniyaa keessatti argamtu, Shikoodraa keessatti dhalatte. Fiiziksiin doktireetii kan fudhatte Yunivarsiitii Paaris 7 irraa yammuu ta’u, yeroo ammaa kana Yunivarsiitii Tiraanaatti Astiroofiiziksiin Pirofeesarummaa guutuu qabdi. Yeroo tokko tokko barattoota ishee waliin ta’uudhaan Teleskooppii ofii isaaniin daawwannaa pilaanetootaa, urjiilee fi gaalaaksotaa ni geggeessu, namootni hedduunis magaalaa Tiraanaa irraa hirmaachuuf baay’ee fedhu. Kitaabota dubbisuu, muuziqaa dhaggeeffachuu ykn sirbuu, naannoo qarqara galaanaa fageenya kilomeetira murtaa’e deemuu fi yeroo mijatu immoo abbaa warraa ishee waliin, addunyaa mara irra imaluu jaallatti. Haaluma wal fakkaatuun, haala siyaasaa biyya ishee keessattis hirmaannaan ishee olaanaa dha. Obboleewwan ishee dubaraa lamaan, hiriyootaa fi gorsitoota ishee dhiyoo ti. Of kennuun yeroo ishee kan dabarsitu pilaanetoota sirna soolaarii ala jiranii irrattii fi hunduu garee saayinsawaa hawaa bara durii dhohiinsa gaammaa reyii waliin, dhimma baay’ee murteessaa kan ta’e, abuuruuf ramadame keessatti ergaan takkaa wal faana hirmaatu.
