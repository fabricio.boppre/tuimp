---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Uniwersytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Pooland
---

Dorootaan Goriiliisis keessatti dhalattee, jireenya ijoollummaa ishee immoo ganda Biisiiziz cina kan taate, Poolaandi xiqqoo, bakka galgala kuula Milkiweyii samii keessaa waliin akka fedhii isheetti bashannanuu dandeessu keessatti dabarsite. Fiiziksii fi Astiroonoomii barachuuf gara Kiraakow deemtee,ergasii magaalaa fi naannoo kana turte. Irra guddeessa ifa optikaalaa irratti ta’us gaalaaksota si’aawoo, keessattuu waa’ee raadiyoo-gaalaaksotaa qo’atti. Isheen ammas taanaan yeroo kamittuu waa’ee samii kanaan kan mirqaantu yoo ta’u, koreen ramaddii sagantaa yeroo barattootaa fi teleskooppii mijeessan wayita isheef hayaman, urjiilee fi gaalaaksota ni daawwatti ykn qoratti. Dhuunfaanis, mirqaana jireenya keessatti ishee quunname ijoollee dhiiraa lakkuu qabdu waliin dabarsuuf yaalti.
