---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de Mexico, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: Meeksikoo
---

Istaan Fiiziksiin doktireetii isaa kan fudhate Yunivarsiitii Wiiskonsiin irraa yoo ta’u, gadi fageenyaan kan inni qorate Astiroofiiziksii fi Raadiyoo-astiroonoomii dha. Yeroo ammaa kana dhaabbata Raadiyoo-astiroonoomii fi Astiroofiiziksii Yunivarsiitii biyyoolessaa of danda’aa Meksiikoo keessatti pirofeesara guutuu dha. Qorannoon isaa uumama urjiilee gurguddoo fi dhiibbaa urjiileen uumaman kun gidduu urjiilee irratti qabaniin kan wal qabatuu dha. Pirojektii qorannoo isaa, digirii jalqabaa irraa kaasee hanga sadarkaa doktoreetiitti kan hojjete, barnoota Fiiziksii sadarkaa digirii jalqabaa fi barnoota Astiroonoomii eebbaa idileen osoo barsiisuu ti. Ijoollummaa isaatti nama suura kaasu fi qonnaan bulaa ture; akkasumas waa hoofuun (konkolaachisuun) si’aayina qaba ture. Yeroo boqonnaa argatu kamittuu nyaata hojjechuu, biqiltuu dhaabuu, biiraa naquu, bal’inaan dubbisuu (addatti yeroo biiraa dhugu), afaanota biroo barachuu, fi guyyaa hunda yeroo muraasa rafuun dabarsa.
