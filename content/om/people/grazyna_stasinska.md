---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Faransaay
---

Giraaziinaan Faransaay keessatti maatii Poolaandi irraa dhalattee,yeroo dheeraaf Paaris keessa jiraachaa turte.Hayyuu Astiroofiiziksii fi miseensa Buufata Qorannoo Hawaa Paarisii ti. Gadi fageenyaan kan isheen qo’atte, waa’ee nebulaa fi gaalaaksotaa ti. Baay’inaan hayyoota Astiroofiiziksii Poolaandi, Meksiikoo,Biraazilii fi Ispeen waliin hojjechaa turtee, boodarra baay’ee michooman.Yeroo boqonnaa isheetti wantoota hedduu hojjetti:kanneen akka afaanota barachuu ykn nyaata kutaalee addunyaa maraa hojjechuu. Akkasumas kitaabota dubbisuu fi muuziqaa kutaalee addunyaa maraa dhaggeefachuu jaallatti. Abbaan warraa ishee hayyuu Astiroofiiziksii ti. Mucaa dubaraa tokko, akkawoo dhiiraa lama (mucaa, mucaa isaanii) fi adurreewwan sadii qabu.
