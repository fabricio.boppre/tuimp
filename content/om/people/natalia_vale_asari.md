---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Biraazil
---

Naataaliyaan biyya Biraaziil, Biraasiliyaatti dhalatte. Nyaata gosa hundaa, fi kobbeewwan, qubeessaa fi waraqaa gosa kamiyyuu yaaluu ni jaallatti. Yeroo kami iyyuu muuziqaa kan ittiin waraabduu fi kitaaba qabattee deemuu jaallatti. Abbaan warraa ishee amma illee waa’ee muuziqaa, kitaabilee fi fiilmiwwanii kanaaf ishee dinqisiifata, akkasumas akka bineensota manaatti simbiroota muraasa qabu. Biyyoota Biraasiiliyaa fi Naataal, Biraaziil, Paaris keessa, Faransaayii, fi Kaambiriij keessa, fi Ingilaandii jiraatteetti. Yeroo ammaa kana Filooriyaanooppoolisii keessa kan jiraattu yoo ta’u, Yuniversidaadee Federaal de Saantaatti Fiiziksii barsiisti. Qorannoon ishee Astiroofiiziksii gaalaaksotaa irratti dha.
