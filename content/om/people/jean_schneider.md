---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Faransaay
---

Maayikiroofiiziksiin erga hojjechaa turee booda, Jiin Shinaayidar caalaatti tilmaama gara qabatamaatti jijjiiruu kan isa dandeessisu, Astiroonoomiitti tarkaanfate. Waa’ee gaalaaksotaa erga qoratee booda, gara pilaanetoota sirna soolaariin ala jiranii fi qorannoo jireenya naannoo urjiilee birootti darbe. Pilaanetoota sirna soolaariin ala jiran saatalaayitii ‘CoRoT’ jedhamu fayyadamuudhaan hubannoo jalqabaa kan uumee fi pilaanetoota aduu ykn urjiilee lamaa tooftaa ittiin barbaadan kallattii kaa’e. Falaasama afaanii, fi gaaffii hariiroowwan yaadaa ykn sammuu fi qaama gidduu jiru irratti fedhii qaba. Yeroo boqonnaa isaatti, bulbula bishaanii irraa halluu hojjeta.
