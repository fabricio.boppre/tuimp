---
name: Astronomsko društvo Sirius
country: Alžirija
---

**Prevajalci:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Znanstveni nadzor:**  
Jamal Mimouni  
Hichem Guergouri

**Jezikovni nadzor:**  
Hafsa Bourkab
