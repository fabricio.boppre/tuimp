---
name: Gloria Delgado Inglada
institution:
  name: Universidad Autonoma de Mexico
  slug: universidad_nacional_autonoma_de_mexico
  country: Mehika
---

[Gloria](http://www.gloriadelgadoinglada.com/) se je rodila v Madridu v Španiji, mesto, ki ga ima zelo rada. Leta 2004 se je preselila v Mehiko zaradi študija astronomije in od takrat živi v tej državi, najprej v Puebli in zdaj v Mexico Cityju. Njene raziskave so osredotočene predvsem na ionizirane meglice, ki jih uporablja za spoznavanje nastanka in razvoja elementov v galaksijah. Gloria rada bere in piše, potuje ter se ure in ure sprehaja in igra s svojim čudovitim psom Anequijem. Zelo dejavna je pri popularizaciji znanosti, vključno s [podkasti](http://aficionporlaciencia.blogspot.mx/), izdajanjem biltena "El Búho azul“ in pisanjem poljudnih znanstvenih člankov.
