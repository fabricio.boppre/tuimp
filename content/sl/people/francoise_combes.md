---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francija
---

Françoise Combes je zelo znana francoska astrofizičarka, ki dela na pariškem observatoriju. Njena številna dela o nastanku in razvoju galaksij v kozmološkem kontekstu s teoretičnega in opazovalnega vidika so ji prinesla več mednarodnih nagrad in zlato medaljo CNRSa. Françoise pogosto predava in je napisala več poljudnih knjig. Rada potuje in ima bogato družinsko življenje s tremi sinovi in sedmimi vnuki.
