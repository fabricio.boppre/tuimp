---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autonoma de Mexico
  slug: universidad_nacional_autonoma_de_mexico
  country: Mehika
---

Stan je doktoriral iz fizike na Univerzi v Wisconsinu s specializacijo iz astrofizike in radioastronomije. Trenutno je redni profesor na Inštitutu za radioastronomijo in astrofiziko Nacionalne avtonomne univerze v Mehiki. Raziskuje kako nastajajo masivne zvezde in njihov vpliv na medzvezdno snov. Vodil je diplomska dela od dodiplomske do doktorske ravni ter redno poučuje dodiplomske fizikalne in podiplomske astronomske predmete. Na začetku življenja je bil fotograf, kmet in navdušenec nad avtomobili. Če bi imel kaj prostega časa, bi ga lahko izkoristil za kuhanje, vrtnarjenje, varjenje piva, branje (po možnosti ob pitju piva), učenje drugih jezikov in vsakodnevno spanje.
