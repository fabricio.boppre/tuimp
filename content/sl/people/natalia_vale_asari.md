---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brazilija
---

Natalia se je rodila v mestu Brasilia v Braziliji. Rada potuje in poskuša vse vrste hrane ter vse vrste pisal, svinčnikov in papirja. Rada ima vedno pri roki glasbeno ploščo in knjigo. Mož jo preseneča s dodatno glasbo, s knjigami in filmi, kot hišne ljubljenčke pa imata nekaj ptic, ki prosto gnezdijo. Živela je v Brasilii in Natalu v Braziliji, Parizu v Franciji in Cambridgeu v Angliji. Zdaj živi v Florianopolisu in poučuje fiziko na Universidade Federal de Santa Catarina. Raziskuje astrofiziko galaksij.
