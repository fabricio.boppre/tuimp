---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Universytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Poljska
---

Dorota se je rodila v Gorlicah, otroštvo pa je preživela v vasi blizu Biecza na Mali Poljski, kjer je lahko brez omejitev uživala v temnem nebu s trakom Mlečne ceste. Zaradi študija fizike in astronomije se je preselila v Krakov in od takrat je navezana na to mesto in okolico. Raziskuje aktivne galaksije, zlasti v radio valovih, čeprav večinoma v optični svetlobi. Nebo jo še vedno očara in kadarkoli ji za to dopuščajo čas študenti in odbori za dodeljevanje časa za teleskop, opazuje zvezde in galaksije. Zasebno poskuša prevzeti nadzor nad sladkim kaosom, ki sta ga v njeno življenje vnesla sinova dvojčka.
