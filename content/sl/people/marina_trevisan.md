---
name: Marina Trevisan
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazilija
---

Marina se je rodila v Curitibi, odraščala pa je v Itajaiju in živela v več drugih brazilskih mestih. Živela je tudi v Londonu (Anglija) in Parizu (Francija). Danes živi v Porto Alegreju ter je učiteljica in raziskovalka na Zvezni univerzi Rio Grande do Sul. Njene raziskave se osredotočajo na nastanek in razvoj galaksij. Astronomija jo navdušuje že od otroštva in še vedno izgubi sapo, ko vidi temno, zvezdnato nebo. Prijatelji jo poznajo po tem, da je nora na mačke, vendar ima zaradi pomanjkanja prostora le štiri. Je poročena in ima hčerko.
