---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francija
---

Po začetnem delu na področju mikrofizike se je Jean Schneider posvetil astronomiji, kjer ima domišljija večjo vlogo. Od galaksij je prešel k zunajosončnim planetom in iskanju življenja okoli drugih zvezd. Bil je pobudnik odkrivanja zunajosončnih planetov s satelitom CoRoT in predlagal metodo za iskanje planetov z dvema soncema. Hkrati ga zanimata filozofija jezika ter odnos med telesom in umom. Pred kratkim se je začel ukvarjati tudi s slikanjem akvarela.
