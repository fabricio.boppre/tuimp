---
name: Julieta Fierro
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Meksikë
---

Julieta Fierro dela na Inštitutu za astronomijo Nacionalne avtonomne univerze v Mehiki in se posveča popularizaciji znanosti. Delala je v muzejih, oblikovala znanstvene delavnice, pisala knjige in članke, snemala televizijske serije in predavala. Pogosto sodeluje v radijskih in televizijskih oddajah. Meni, da je popularizacija znanosti del neformalnega izobraževanja, tj. vseživljenjskega izobraževanja. V prostem času rada bere in igra flavto.