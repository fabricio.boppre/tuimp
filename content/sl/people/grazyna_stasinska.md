---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francija
---

Grażyna se je rodila v Franciji, kot otrok poljskih staršev in večino svojega življenja je preživela v Parizu. Je astrofizičarka in članica pariškega observatorija. Specializirana je za meglice in galaksije. Sodelovala je predvsem s poljskimi, mehiškimi, brazilskimi in španskimi astrofiziki, ki so postali njeni tesni prijatelji. Ima številne hobije, kot sta učenje jezikov in kuhanje jedi z vsega sveta. Prav tako rada bere knjige in posluša glasbo z vseh koncev sveta. Tudi njen mož je astrofizik. Imata hčerko, dva vnuka in tri mačke.
