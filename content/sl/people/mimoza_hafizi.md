---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albanija
---

Mimoza se je rodila v Shkodri, zgodovinskem in zelo lepem mestu na severu Albanije. Doktorirala je iz fizike na Univerzi Paris 7 in je trenutno redna profesorica astrofizike na Univerzi v Tirani. Včasih s svojimi študenti izvaja opazovanja planetov, zvezd in galaksij z lastnim teleskopom, pri katerih z veseljem sodelujejo številni prebivalci mesta Tirana. Rada bere knjige, posluša glasbo ali poje, hodi na kilometre ob morju in z možem potuje po svetu, kadar je le mogoče. Hkrati je zelo dejavna v političnem življenju svoje države. Njeni hčeri sta najboljši prijateljici in svetovalki. Posebej se posveča zunajsončnim planetom, hkrati pa sodeluje v znanstveni skupini, ki se ukvarja z odkrivanjem zelo starega vesolja z izbruhi gamma žarkov, ki so najbolj energijski dogodki v vesolju.