---
name: Rogério Riffel
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazilija
---

Rogerio se je rodil v Alegriji, majhnem mestu na podeželju brazilskega mesta Rio Grande do Sul. Z bratom, prav tako astrofizikom, je vedno rad opazoval nebo in ga poskušal razumeti na jutranjih sprehodih v šolo. Včasih je na dan prehodil 12 km, da je prišel v razred. Diplomiral je iz fizike, magistriral in doktoriral iz astrofizike. Danes je profesor in opravlja znanstvene raziskave na Zvezni univerzi Rio Grande do Sul. Njegove raziskave so povezane z aktivnimi galaksijami in njihovimi supermasivnimi črnimi luknjami. Rad občuduje nebo, igra nogomet, goji rastline za prehrano, dela pivo in pije mate. Je poročen in ima dva otroka.
