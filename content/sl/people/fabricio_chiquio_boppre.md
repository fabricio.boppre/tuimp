---
name: Fabricio Chiquio Boppré
---

Fabricio se je rodil in živi na otoku Santa Catarina v mestu Florianópolis na jugu Brazilije. Njegova glavna strast je glasba, rad pa tudi potuje (na koncerte) in ob večerih doma kuha (ob poslušanju svoje zbirke plošč). Ker je poročen z astrofizičarko, jo vedno sprašuje, kaj je obstajalo pred velikim pokom. Meni, da mu še ni zadovoljivo odgovorila, zato bo še naprej spraševal. Med njegovimi vsakodnevnimi opravili so še literatura, videoigre, grozljivke, plavanje in sprehod do plaže. Ko ima dovolj časa, se ukvarja z razvojem spletnih strani. Če želite izvedeti več o njem, kliknite [tukaj](http://www.fabricioboppre.net/).
