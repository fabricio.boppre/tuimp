---
name: Frédéric Vicent
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francija
---

Frédéric Vicent je raziskovalec na pariškem observatoriju. Med drugim se ukvarja s črnimi luknjami. Je član obsežnega sodelovanja pri Event Horizon Telescope, ki je leta 2019 objavil prvo sliko sence črne luknje.
