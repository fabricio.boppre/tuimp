---
name: Georges Alecian
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francija
---

Georges Alecian je francoski astrofizik. Je zaslužni raziskovalni direktor v CNRSu in dela na pariškem observatoriju na področju zvezdne fizike. Njegova raziskovalna dejavnost se osredotoča na kemično sestavo zvezd in zlasti na fizikalne procese, ki ustvarjajo nehomogenosti v porazdelitvi elementov. Sodeluje z astrofiziki iz več držav, predvsem iz Francije, Kanade, Avstrije in Armenije. Njegovi hobiji so fotografija, domače mojstrstvo in visokotehnološki pripomočki. Tudi njegova žena je astrofizičarka. Imata hčerko, tri vnuke in različno število mačk.