---
name: Marina Trevisan
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazil
---

Marina was born in Curitiba, but grew up in Itajaí and has lived in several other cities in Brazil. She has also lived in London (England) and Paris (France). Today she is in Porto Alegre and is a teacher and researcher at the Federal University of Rio Grande do Sul. Her research focuses on the formation and evolution of galaxies. She has been passionate about astronomy since she was a child and still loses her breath when she sees a dark, starry sky. She is known by her friends for being crazy about cats, but she only has four due to lack of space. She is married and has a daughter.
