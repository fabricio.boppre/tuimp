---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Uniwersytet Jagielloñski
  slug: uniwersytet_jagiellonski
  country: Poland
---

Dorota was born in Gorlice, and spent her childhood in a village near Biecz in Lesser Poland, where she could without any restrictions enjoy the dark sky with the ribbon of the Milky Way. She moved to Kraków to study physics and astronomy, and since then she is attached to this city and surroundings. She is studying active galaxies, particularly radio galaxies, although mainly in optical light. She is still charmed by the sky and whenever time, students and telescope time allocation committees allow her, she conducts observations of stars and galaxies. Privately, she tries to take control of the sweet chaos introduced to her life by twin sons.
