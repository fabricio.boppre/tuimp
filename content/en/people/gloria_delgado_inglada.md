---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Mexico
---

[Gloria](http://www.gloriadelgadoinglada.com/) was born in Madrid, Spain, a city that she really loves. She moved to Mexico in 2004 to study astronomy and since then, she has been living in this country, first in Puebla and now in Mexico City. Her research is mainly focused on ionized nebulae, she uses them to learn about the production and evolution of elements in galaxies. Gloria likes to read and write, to travel, and to walk and play for hours with her amazing dog [Anequi](http://www.gloriadelgadoinglada.com/anequi). She is very active in outreach activities, including [podcasts](http://aficionporlaciencia.blogspot.mx/) about science, the edition of the bulletin ["El Búho azul"](http://sam.org.mx/editorial-sam/el-buho-azul.php), and writing outreach articles.
