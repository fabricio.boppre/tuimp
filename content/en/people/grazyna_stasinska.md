---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Grażyna was born in France of Polish parents and has lived in Paris most of her life. She is an astrophysicist and is a member of Paris Observatory. She specializes in nebulae and galaxies. She has mostly worked with Polish, Mexican, Brazilian and Spanish astrophysicists who became close friends. She has many hobbies, such as learning languages or cooking meals from all over the World. She also likes to read books and listen to music from all the parts of the World. Her husband is also an astrophysicist. They have one daughter, two grand sons and three cats.
