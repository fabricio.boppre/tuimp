---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

After working in the field of microphysics, Jean Schneider turned to astronomy, which puts imagination more into play. After studying galaxies, he moved on to exoplanets and the search for life around other stars. He initiated the detection of exoplanets with the CoRoT satellite and proposed a method of searching for planets with two suns. He is also interested in the philosophy of language and the question of relationships between mind and body. As a hobby, he does watercolor painting.
