---
name: Frédéric Vicent
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Frédéric Vicent is a researcher at the Paris Observatory. Among other things, he works on black holes. He is a member of the vast Event Horizon Telescope collaboration, which published the first image of the shadow of a black hole in 2019.
