---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albania
---

Mimoza was born in Shkodra, a historic and very nice city in the northern part of Albania. She received her doctorate in physics from the Paris 7 University and is currently a full professor of Astrophysics at Tirana University. Sometimes with her students she makes observations of planets, stars and galaxies with their own telescope, in which many people from Tirana city are happy to participate. She likes to read books, listen to music or sing, walk for miles by the sea and travel around the world, whenever possible, with her husband. At the same time, she is very active in the political life of her country. Her two daughters are the best friends and advisors. She is particularly devoted to extrasolar planets and at the same time participates in a scientific team dedicated to the discovery of the very old universe with Gamma Ray Bursts, the most energetic events in the Universe.
