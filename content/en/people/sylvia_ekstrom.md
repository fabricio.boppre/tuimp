---
name: Sylvia Ekström
institution:
  name: Observatoire de Genève
  slug: observatoire_de_geneve
  country: Switzerland
---
Sylvia was born in Stockholm and grew up in Geneva. She trained as a midwife and worked in a delivery room for 11 years before leaving everything behind to take up astrophysics after seeing comet Hale-Bopp. A specialist in stellar physics, she is also head of communications for the Department of Astronomy at the University of Geneva. Married with two cats, she devotes her spare time to singing and playing the cello, and gives regular concerts.

