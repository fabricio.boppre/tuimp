---
name: Laura Bernard
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Laura Bernard was born and grew up in Lozère, a beautiful mid-mountain region in the south of France. She is a researcher at the CNRS and works at the Paris Observatory. She specialises in gravitational waves and works on alternatives to general relativity. She is helping to prepare future missions to detect gravitational waves. In her spare time, she also practises athletics at a high level and takes part in long-distance races. When she's not running, she can be found immersed in a book, at the cinema or recharging her batteries in Lozère.
