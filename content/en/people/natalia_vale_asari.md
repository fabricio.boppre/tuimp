---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brazil
---

Natalia was born in Brasilia, Brazil. She likes to travel to try all kinds of food, and all types of pens, pencils and paper. She likes to have a music record and a book always at hand. Her husband surprises her with yet more music, books and films, and they have a few free nesting birds as pets. She has lived in Brasilia and Natal, Brazil, in Paris, France, and in Cambridge, England. She now lives in Florianopolis and teaches physics at Universidade Federal de Santa Catarina. Her research is on the astrophysics of galaxies.
