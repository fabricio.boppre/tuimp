---
name: Julieta Fierro
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: Mexico
---

Julieta Fierro works at the Institute of Astronomy of the National Autonomous University of Mexico, and is dedicated to the popularisation of science. She has worked in museums, designed science workshops, written books and articles, made TV series, given lectures. She frequently participates in radio and television programmes. She considers science popularisation to be part of informal education, i.e. lifelong education. In her spare time he enjoys reading and playing the flute.
