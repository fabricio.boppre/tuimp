---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de México, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: Mexico
---

Stan received his doctorate in physics from the University of Wisconsin, with a specialization in astrophysics and radio astronomy. He is presently a full professor at the Institute of Radioastronomy and Astrophysics of the National Autonomous University of Mexico. His research is in the area of massive star formation and the effects of these stars on the interstellar medium. He has directed thesis projects from the undergraduate to doctoral levels and regularly teaches undergraduate physics and graduate astronomy courses. Earlier in life he was a photographer, a farmer, and a muscle car enthusiast. If he had any free time, he might use it to cook, garden, brew beer, read widely (preferably while drinking beer), learn other languages, and take a nap every day.
