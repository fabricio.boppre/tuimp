---
name: Rogério Riffel
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazil
---

Rogerio was born in Alegria, a small town in the countryside of Rio Grande do Sul in Brazil. He always liked to see the sky and try to understand it on early morning walks to school with his brother, also an astrophysicist. He used to walk 12 km a day to go to class. He has a degree in physics, a master and a doctorate in astrophysics. Today he is a professor and does scientific research at the Federal University of Rio Grande do Sul. His research is related to active galaxies and their supermassive black holes. He enjoys admiring the sky, playing football, growing food plants, making beer and drinking mate. He is married and has two children.
