---
name: Fabricio Chiquio Boppré
---

Fabricio was born and lives on the Island of Santa Catarina, in the city of Florianópolis, south of Brazil. His main passion is music, but he also loves to travel (to attend concerts) and cooking at home in the evenings (while listening to his record collection). Being married to an astrophysicist, he is always asking her what existed before the Big Bang. He considers that she has not answered him satisfactorily yet, and so he will keep asking. Literature, video-game, horror movies, swimming and going to the beach are other of his daily occupations. When there is enough time, he works developing web-sites. To know more about him, click [here](http://www.fabricioboppre.net).
