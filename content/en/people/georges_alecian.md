---
name: Georges Alecian
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Georges Alecian is an astrophysicist in France. He is a CNRS Emeritus Research Director and works at the Paris Observatory in the field of stellar physics. His research activity focuses on the chemical composition of stars and in particular on the physical processes that create inhomogeneities in the distribution of elements. He collaborates with astrophysicists from several countries, mainly France, Canada, Austria and Armenia. His hobbies include photography, DIY and high-tech gadgets. His wife is also an astrophysicist. They have a daughter, three grandsons and a variable number of cats.
