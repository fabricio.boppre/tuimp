---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---

Françoise Combes is a very famous French astrophysicist who works at the Paris Observatory. Her numerous works on the formation and evolution of galaxies in a cosmological context, from both theoretical and observational points of view, have won her several international awards and a gold medal from the National Center for Scientific Research. Françoise often gives outreach lectures and has written several books for the general public. She enjoys traveling and has a rich family life with three sons and seven grandchildren.
