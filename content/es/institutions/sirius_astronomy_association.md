---
name: Asociación de Astronomía Sirius
country: Argelia
---

**Traductores:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Supervisión científica:**  
Jamal Mimouni  
Hichem Guergouri

**Supervisión lingüística:**  
Hafsa Bourkab
