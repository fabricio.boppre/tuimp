---
name: Frédéric Vicent
institution:
  name: Observatoire de París
  slug: observatoire_de_paris
  country: Francia
---

Frédéric Vicent es investigador en el Observatorio de París. Entre otras cosas, trabaja sobre los agujeros negros. Es miembro de la vasta colaboración Event Horizon Telescope, que publicó la primera imagen de la sombra de un agujero negro en 2019.
