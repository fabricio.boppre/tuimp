---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francia
---

Françoise Combes es una astrofísica francesa muy famosa. Trabaja en el Observatorio de París. Sus numerosos trabajos sobre la formación y evolución de las galaxias en un contexto cosmológico, tanto desde un punto de vista teórico como observacional, le han valido varios premios internacionales así como la medalla de oro del Centro Nacional de Investigaciones Científicas. Françoise frecuentemente imparte conferencias abiertas a todos y ha escrito varios libros para el público en general. Le gusta mucho viajar. Su vida familiar también es muy rica, con tres hijos y siete nietos.
