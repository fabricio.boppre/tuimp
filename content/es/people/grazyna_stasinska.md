---
name: Grażyna Stasińska
institution:
  name: Observatoire de París
  slug: observatoire_de_paris
  country: Francia
---

Grażyna​ nació en Francia, tiene padres polacos y ha vivido en P​aris la mayor parte de su vida. Es astrofísica y es miembro del Observatorio de Paris. Es especialista en nebulosas y galaxias. Ha trabajado principalmente con astrónomos polacos, mexicanos, brasileños y españoles; de los que se ha vuelto una amiga muy cercana. Tiene muchas aficiones como aprender idiomas y cocinar platillos de todo el mundo. También le gusta mucho leer y escuchar música de todos los países. Su esposo es también astrofísico. Tienen una hija, dos nietos y tres gatos.
