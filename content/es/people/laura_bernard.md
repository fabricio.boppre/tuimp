---
name: Laura Bernard
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Laura Bernard nació y creció en Lozère, una hermosa región de media montaña del sur de Francia. Es investigadora del CNRS y trabaja en el Observatorio de París. Está especializada en ondas gravitacionales y trabaja en alternativas a la relatividad general. Colabora en la preparación de futuras misiones de detección de ondas gravitacionales. En su tiempo libre también practica atletismo de alto nivel y participa en carreras de larga distancia. Cuando no corre, se la puede encontrar inmersa en un libro, en el cine o recargando pilas en Lozère.
