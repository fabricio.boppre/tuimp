---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Universytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Polonia
---

Dorota nació en Gorlice y pasó su niñez en un pueblo cerca de Biecz en la conocida como Pequeña Polonia, donde podía disfrutar sin restricciones del cielo oscuro con la Vía Láctea. Se mudó a Cracovia para estudiar física y astronomía, y desde entonces está ligada a esta ciudad y sus alrededores. Ella estudia galaxias activas, principalmente radiogalaxias, sobre todo en el rango visible. Todavía sigue encantada por el cielo y cuando el tiempo, los estudiantes y los comités de asignación de tiempo de telescopio se lo permiten, realiza observaciones de estrellas y galaxias. En lo personal, ella intenta tener el control del dulce caos que sus hijos gemelos han traído a su vida.
