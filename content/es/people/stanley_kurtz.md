---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de México, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: México
---

Stan recibió su doctorado en física de la Universidad de Wisconsin, con la especialidad en astrofísica y radioastronomía. Actualmente es profesor de tiempo completo en el Instituto de Radioastronomía y Astrofísica de la Universidad Nacional Autónoma de México. ​Su investigación se enfoca en la formación de estrellas masivas y los efectos de estas estrellas en el medio interestelar. Ha dirigido proyectos de tesis a nivel de licenciatura y posgrado y da clases de física en la licenciatura y astrofísica en la maestría. En el pasado fue fotógrafo, granjero y fanático de los "muscle cars". Si tuviera tiempo libre, le gustaria cocinar, arreglar el jardín, elaborar cerveza, leer mucho (de preferencia mientras bebe cerveza), aprender otros idiomas y tomar una siesta diaria.
