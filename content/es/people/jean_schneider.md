---
name: Jean Schneider
institution:
  name: Observatoire de París
  slug: observatoire_de_paris
  country: Francia
---

Después de trabajar en microfísica, Jean Schneider dirigió su atención a la astronomía, un campo que deja volar la imaginación. Empezó analizando galaxias y continuó con el estudio de exoplanetas y la búsqueda de vida alrededor de otras estrellas. Es un pionero de la detección de exoplanetas, definiendo el caso científico de la misión CoRoT y proponiendo la búsqueda de planetas en sistemas estelares con dos soles. Al mismo tiempo su interés se extiende a la filosofía del lenguaje y a la relación entre el cuerpo y la mente. Desde hace algún tiempo también pinta con acuarelas.
