---
name: Marina Trevisan
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brasil
---

Marina nació en Curitiba, pero creció en Itajaí y ha vivido en otras ciudades de Brasil. También ha vivido en Londres (Inglaterra) y París (Francia). Actualmente se encuentra en Porto Alegre y es profesora e investigadora en la Universidad Federal de Rio Grande do Sul. Su investigación se centra en la formación y evolución de las galaxias. Es una apasionada de la astronomía desde que era niña y todavía pierde el aliento cuando ve un cielo oscuro y estrellado. Es conocida por sus amigos por estar loca por los gatos, pero sólo tiene cuatro por falta de espacio. Está casada y tiene una hija.
