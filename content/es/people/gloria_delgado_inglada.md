---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: México
---

[Gloria](http://www.gloriadelgadoinglada.com/) nació en Madrid, España, una ciudad que le encanta. Se mudó a México en 2004 para estudiar astronomía y desde entonces, vive en este país, primero en Puebla y ahora en la Ciudad de México. Su investigación se centra en las nebulosas ionizadas, las utiliza para aprender sobre la producción y evolución de los elementos en las galaxias. Le gusta leer y escribir, viajar, y caminar y jugar durante horas con su increíble perro [Anequi](http://www.gloriadelgadoinglada.com/anequi). Es muy activa en la divulgación, hace [podcasts](http://aficionporlaciencia.blogspot.mx/) de ciencia, edita un [boletín de astronomía](http://sam.org.mx/editorial-sam/el-buho-azul.php) y escribe artículos de divulgación.
