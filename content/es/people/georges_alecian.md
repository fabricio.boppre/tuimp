---
name: Georges Alecian
institution:
  name: Observatoire de París
  slug: observatoire_de_paris
  country: Francia
---

Georges Alecian es un astrofísico francés. Es director de investigación emérito del CNRS y trabaja en el Observatorio de París en el campo de la física estelar. Su actividad de investigación se centra en la composición química de las estrellas y, en particular, en los procesos físicos que crean inhomogeneidades en la distribución de los elementos. Colabora con astrofísicos de varios países, principalmente Francia, Canadá, Austria y Armenia. Sus aficiones son la fotografía, el bricolaje y los aparatos de alta tecnología. Su esposa también es astrofísica. Tienen una hija, tres nietos y un número variable de gatos.
