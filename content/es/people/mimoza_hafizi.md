---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albania
---

Mimoza nació en Shkodra, una ciudad histórica muy agradable en la parte norte de Albania. Recibió su doctorado en física de la Universidad Paris 7 y actualmente es profesora titular de Astrofísica en la Universidad de Tirana. A veces, con sus alumnos, realiza observaciones de planetas, estrellas y galaxias con su propio telescopio, y muchas personas de la ciudad de Tirana también están encantados de participar. Le gusta leer, escuchar música y cantar, caminar kilómetros a la orilla del mar y viajar por todo el mundo con su esposo. Es muy activa en la vida política de su país. Sus dos hijas son sus mejores amigas y consejeras. En sus investigaciones ella trabaja sobre los planetas extrasolares y forma también parte de un grupo dedicado a descubrir el universo muy temprano con explosiones de rayos gamma, los eventos más energéticos en el Universo.
