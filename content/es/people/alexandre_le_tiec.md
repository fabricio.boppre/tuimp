---
name: Alexandre Le Tiec
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Alexandre es investigador del CNRS y trabaja en el Observatorio de París en el campo de la astrofísica de altas energías. Sus investigaciones se centran principalmente en la física de los agujeros negros y las ondas gravitacionales generadas por pares de estrellas compactas. Recientemente se ha apasionado por la historia de la vida en la Tierra y la de nuestra especie, el Homo sapiens. Paralelamente a su investigación astrofísica, está llevando a cabo una investigación que abarca las ciencias naturales, humanas y sociales, con el objetivo de rastrear las raíces del Antropoceno. En su tiempo libre, lee libros, escucha música y sale a pasear, a veces al mismo tiempo.
