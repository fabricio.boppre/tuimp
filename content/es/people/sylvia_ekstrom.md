---
name: Sylvia Ekström
institution:
  name: Observatoire de Genève
  slug: observatoire_de_geneve
  country: Suiza
---
Sylvia nació en Estocolmo y creció en Ginebra. Se formó como comadrona y trabajó en una sala de partos durante 11 años antes de dejarlo todo para dedicarse a la astrofísica tras ver el cometa Hale-Bopp. Especialista en física estelar, es también responsable de comunicación del Departamento de Astronomía de la Universidad de Ginebra. Casada y con dos gatos, dedica su tiempo libre a cantar y tocar el violonchelo, y da conciertos regularmente.
