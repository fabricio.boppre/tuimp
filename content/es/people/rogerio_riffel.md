---
name: Rogério Riffel
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brasil
---

Rogerio nació en Alegría, una pequeña ciudad del campo de Rio Grande do Sul, en Brasil. Siempre le gustó ver el cielo y tratar de entenderlo en los paseos matutinos a la escuela con su hermano, también astrofísico. Solía caminar 12 km al día para ir a clase. Es licenciado en física, tiene un máster y un doctorado en astrofísica. Hoy es profesor y realiza investigaciones científicas en la Universidad Federal de Rio Grande do Sul. Sus investigaciones están relacionadas con las galaxias activas y sus agujeros negros supermasivos. Le gusta admirar el cielo, jugar al fútbol, cultivar plantas alimenticias, hacer cerveza y tomar mate. Está casado y tiene dos hijos.
