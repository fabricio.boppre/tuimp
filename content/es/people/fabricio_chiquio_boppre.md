---
name: Fabricio Chiquio Boppré
---

Fabricio nació y vive en la isla de Santa Catarina, en la ciudad de Florianópolis, al sur de Brasil. Su pasión principal es la música pero también le gusta viajar (para ir a conciertos) y cocinar por la noche en casa (mientras escucha su colección de discos). Casado con una astrofísica, siempre le ha preguntado a ella que había antes del Big Bang. Como considera que ella aún no le ha contestado de forma satisfactoria, aún le sigue haciendo la misma pregunta. La literatura, los video-juegos, las películas de terror, nadar e ir a la playa son otras de sus ocupaciones diarias. Cuando tiene suficiente tiempo, ​desarrolla páginas webs. Para saber más sobre él, haz click [aquí](http://www.fabricioboppre.net).
