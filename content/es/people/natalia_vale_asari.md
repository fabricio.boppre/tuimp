---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brasil
---

Natalia nació en Brasilia, Brasil. Le gusta viajar y probar todo tipo de comida, y todo tipo de plumas, lápices y papeles. ​Siempre tiene a mano música y un libro. Su esposo la sorprende con más música, libros y películas, y ellos tienen como mascotas unos cuantos pájaros libres. Ella ha vivido en Brasilia y Natal, en Brasil, en Paris, Francia, y en Cambridge, Inglaterra. Ahora vive en Florianópolis y enseña física en la Universidad de Santa Catarina. Su investigación es acerca de la astrofísica galáctica.
