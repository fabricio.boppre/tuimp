---
name: Julieta Fierro
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: México
---

Julieta Fierro trabaja en el Instituto de Astronomía de la Universidad Nacional Autónoma de México, se dedica a la divulgación de la ciencia. Ha trabajado en museos, ideado talleres de ciencia, escrito libros y artículos, realizado series de televisión, impartido conferencias. Con frecuencia participa en programas de radio y televisión. Considera la divulgación de la ciencia es parte de la educación informal, es decir educación de por vida. En su tiempo libre le gusta leer y tocar la flauta.
