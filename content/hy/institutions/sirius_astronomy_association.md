---
name: Սիրիուսի աստղագիտության ասոցիացիա
country: Ալժիր
---

**Translators:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Scientific supervision:**  
Jamal Mimouni  
Hichem Guergouri

**Linguistic supervision:**  
Hafsa Bourkab
