---
name: Ջիմմայի համալսարան
country: Եթովպիա
---

Abdisa Tesema - MSc. in Astrophysics  
Bikila Teshome - MSc. in Astrophysics  
Jifar Raya - MSc. in Astrophysics  
Sena Bokona - MSc. in Astrophysics  
Tolu Biressa - PhD in Astrophysics
