---
name: Shoqata e Astronomisë Sirius
country: Algjeri
---

**Përkthyes:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Mbikëqyrja shkencore:**  
Jamal Mimouni  
Hichem Guergouri

**Mbikëqyrja gjuhësore:**  
Hafsa Bourkab
