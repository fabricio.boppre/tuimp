---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Shqipëri
---

Mimoza ka lindur në Shkodër, një qytet historik shumë i këndshëm në veri të Shqipërisë. Ka fituar doktoratën në fizikë në Universitetin Paris 7 dhe tani është profesore astrofizike në Universitetin e Tiranës. Nganjëherë, bashkë me studentët organizojnë vrojtime të planeteve, yjeve dhe galaksive me teleskopin e tyre, ku lumturohen të marrin pjesë shumë njerëz nga qyteti i Tiranës. I pëlqen të lexojë libra, të dëgjojë a këndojë muzikë, të ecë me kilometra buzë detit dhe kur është e mundur të udhëtojë në të gjithë botën me burrin e vet. Ndërkohë është shumë aktive në jetën politike të vendit. Dy vajzat e saj janë miket dhe këshilltaret më të mira. Kërkimin shkencor ia kushton kryesisht planeteve jashtëdiellore. Ajo merr pjesë në një projekt të quajtur THESEUS, për të zbuluar universin shumë të hershëm përmes Shpërthimeve Gama, ngjarjeve më energjike në Univers.
