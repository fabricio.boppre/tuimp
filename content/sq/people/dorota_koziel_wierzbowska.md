---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Universytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Poloni
---

Dorota ka lindur në Gorlice dhe ka kaluar fëmijërinë në një fshat pranë Biecz në Lesser Poloni, ku ajo ka mundur të shijojë pa kufi qiellin e errët me fjongon e Rrugës së Qumështit. Më pas ka shkuar në Kraków për për të studiuar fizikë dhe astronomi, dhe që atëhere është lidhur me këtë qytet dhe rrethinat. Ajo studion galaksitë aktive, veçanërisht radio-galaksitë, ndonëse në dritë e dukshme. Ende mahnitet nga qielli e kur ka kohë që ia falin studentët dhe komiteti i programimit të teleskopit, ndjek e vrojton yjet dhe galaksitë. Privatisht mundohet të mbajë nën kontroll kaosin e këndshëm që ia sjellin në jetë dy djemtë e saj binjakë.
