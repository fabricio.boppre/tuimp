---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francë
---

Grażyna ka lindur në Francë nga prindër polakë dhe ka jetuar në Paris kohën më të madhe të jetës së saj. Ajo është astrofizikane dhe pjesë e Observatorit të Parisit. Specialiteti i saj janë mjegullnajat në galaksi. Më shumë ka punuar me astrofizikanë polakë, meksikanë, brazilianë dhe spanjollë, që janë bërë dhe miq të ngushtë. Ajo ka shumë hobby, si mësimi i gjuhëve apo gatimi i ushqimeve nga e gjithë bota. I pëlqen të lexojë libra dhe të dëgjojë muzikë nga e gjithë bota. Burrin e ka gjithashtu astrofizikan. Ata kanë një vajzë, dy nipa dhe tre mace.
