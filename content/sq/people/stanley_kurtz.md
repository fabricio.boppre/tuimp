---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autonoma de Mexico
  slug: universidad_nacional_autonoma_de_mexico
  country: Meksikë
---

Stan ka fituar doktoratën në fizikë nga Universiteti Wisconsin, me një specializim në astrofizikë dhe radio astronomi. Tani është profesor në Institutin e Astronomisë dhe Astrofizikës në Universitetin autonom kombëtar të Mexico. Kërkimin shkencor e ka në fushën e formimit të yjeve masive dhe efektin që këto yje japin në mjedisin ndëryjor. Ka drejtuar teza nga niveli universitar në atë doktoral dhe jep leksione fizike dhe astronomie në universitet. Më parë ka qenë fotograf, fermer dhe amator i madh veturash amerikane. Kur ka kohë të lirë, ai gatuan, punon kopështin, prodhon birrë, lexon shumë (për qejf me një gotë birrë përpara), mëson gjuhë të tjera dhe bën çdo ditë gjumin e drekës.
