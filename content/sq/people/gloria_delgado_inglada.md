---
name: Gloria Delgado Inglada
institution:
  name: Universidad Autonoma de Mexico
  slug: universidad_nacional_autonoma_de_mexico
  country: Meksikë
---

[Gloria](http://www.gloriadelgadoinglada.com/) ka lindur në Madrid, Spanjë, qytet që ajo e adhuron. Ka ikur në Meksikë në 2004 për të studiuar astronomi dhe që atëhere jeton në këtë vend, fillimisht në Puebla dhe tani në Mexico. Kërkimet e saj zhvillohen kryesisht mbi mjegullnajat e jonizuara. Për këtë arsye ajo mund të studiojë prodhimin e elementeve kimike dhe evolucionin e tyre në galaksi. Glorias i pëlqen të lexojë dhe të shkruajë, të udhëtojë, ecë dhe të luajë me orë me qenin e saj Anequi. Ajo i kushton shumë kohë përhapjes së shkencës, merr pjesë në [podkaste](http://aficionporlaciencia.blogspot.mx/), boton buletinin "El Búho azul" (Bufi blu) dhe shkruan artikuj për popullarizimin e shkencës.
