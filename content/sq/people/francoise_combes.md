---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francë
---

Françoise Combes është një astrofizikane shumë e famshme franceze. Ajo punon në Observatorin e Parisit. Për punimet e saj të shumta mbi formimin dhe evolucionin e galaktikave në një kontekst kozmologjik, si nga pikëpamja teorike dhe nga vëzhgimi, ka fituar disa çmime ndërkombëtare, si dhe medaljen e artë nga Qendra Kombëtare e Kërkimit Shkencor. Françoise shpesh jep leksione të hapura për të gjithë dhe shkruan libra për publikun e gjerë. Asaj i pëlqen shumë të udhëtojë. Jeta e saj familjare është gjithashtu mjaft e pasur, me tre djem dhe shtatë nipër e mbesa.
