---
name: Marina Trevisan
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazil
---

Marina ka lindur në Curitiba, por është rritur në Itajaí dhe ka jetuar në disa qytete të tjera të Brazilit. Ka jetuar edhe në Londër (Angli) dhe në Paris (Francë). Sot është në Porto Alegre dhe jep mësim e kryen kërkim shkencor në Federal University of Rio Grande do Sul. Kërkimin e ka fokusuar drejt formimit dhe evolucionit të galaktikave. Marina e ka dashur astronominë që e vogël dhe ende mbetet pa frymë kur sheh një qiell të errët, mbushur me yje. Miqtë e saj e dinë se sa e çmendur është pas maceve, por për mungesë vendi mban vetëm katër. Është e martuar, me një fëmijë.
