---
name: Georges Alecian
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francë
---

Georges Alecian është astrofizikan në Francë. Ai mban pozicionin Drejtor Kërkimi Emeritus në CNRS dhe punon në Observatorin e Parisit, në fushën e fizikës yjore. Aktiviteti i tij kërkues fokusohet në përbërjen kimike të yjeve e në veçanti në fizikën e proceseve që krijojnë johomogjenitete në shpërndarjen e elementeve. Georges bashkëpunon me astrofizikanë nga vende të ndryshme, kryesisht nga Franca, Kanadaja, Austria dhe Armenia. Në hobet e tij përfshihen fotografia dhe lojrat DIY e high-tech. Edhe gruaja e tij është astrofizikane. Ata kanë një vajzë, tre nipa dhe një numër variabël macesh.
