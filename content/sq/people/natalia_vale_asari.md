---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brazil
---

Natalia ka lindur në Brazilia, Brazil. I pëlqen të udhëtojë, për të provuar të gjitha llojet e ushqimit dhe të gjitha tipet e lapsave, stilolapave dhe letrave. I pëlqen të ketë gjithmonë në dorë një CD muzike dhe një libër. Bashkëshorti i dhuron shpesh libra dhe CD muzike a filmash, dhe ata kanë disa fole me zogj në shtëpinë e tyre. Ajo ka jetuar në Brazilia dhe Natal, Brazil, në Paris, Francë dhe në Cambrige, Angli. Tani jeton në Florianopolis dhe jep leksione fizike në Universidade Federal në Santa Catarina. Kërkimi i saj shkencor është mbi astrofizikën dhe galaksitë.
