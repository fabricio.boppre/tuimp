---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: Francë
---

Pas punës së tij në mikrofizikë, Jean Schneider iu drejtua astronomisë, e cila kërkon më shumë imagjinatë. Nga galaksitë kaloi në ekzoplanetet dhe kërkimin e jetës rreth yjeve. Ai ka qenë nismëtari i zbulimit të ekzoplaneteve me satelitin CoRoT dhe ka propozuar një metodë për kërkimin e planeteve me dy diej. Paralelisht, është i interesuar ndaj filozofisë së shprehjes dhe raporteve mes trupit e mëndjes. Së fundmi ka filluar të pikturojë në akuarel.
