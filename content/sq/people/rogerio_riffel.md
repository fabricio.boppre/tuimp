---
name: Rogério Riffel
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brazil
---

Rogerio ka lindur në Alegria, qytet i vogël në periferi të Rio Grande do Sul në Brazil. Gjithmonë i ka pëlqyer të kundrojë qiellin e të përpiqet ta kuptojē, gjatë rrugës për në shkollë, kur shkonte ende pa gdhirë me vëllain e tij, gjithashtu astrofizikan. Bënin 12 km në ditë për të vajtur në mësim. Rogerio ka një diplomë në fizikë, një master dhe një doktoratë në astrofizikë. Aktualisht është profesor dhe bën kërkim shkencor në Federal University of Rio Grande do Sul. Kërkimi i tij lidhet me vrimat e zeza supermasive. Kënaqet duke admiruar qiellin, duke luajtur futboll, duke mbjellë bimë ushqimore, e duke prodhuar birrë për ta pirë me shokët. Është i martuar dhe ka dy fëmijë.
