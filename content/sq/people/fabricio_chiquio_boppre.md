---
name: Fabricio Chiquio Boppré
---

Fabricio ka lindur dhe ka jetuar gjithmonë në Florianópolis, në ishullin e Santa Catarina, në jug të Brazilit. Pasioni i tij më i madh është muzika, por i pëlqen gjithashtu të udhëtojë (për të shkuar në koncerte) dhe në darkë të gatuajë në shtëpi (duke dëgjuar koleksionin e tij të CD). I martuar me një astrofizikane, ai nuk pushon së kërkuari gruas së tij se ç’ka patur para Big Bang-ut. Meqë mendon se ajo nuk i është përgjigjur ende në mënyrë të kënaqshme, ai do vazhdojë t’ia kërkojë. Letërsia, lojrat video, filmat horror, noti dhe shëtitjet në plazh janë mes angazhimeve kryesore. Kur ka kohë, ngre faqe interneti. Për të mësuar më shumë për të, shtypni [këtu](http://www.fabricioboppre.net/).
