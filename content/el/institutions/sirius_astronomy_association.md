---
name: Σύλλογος Αστρονομίας Sirius
country: Αλγερία
---

**Μεταφραστές:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Επιστημονική εποπτεία:**  
Jamal Mimouni  
Hichem Guergouri

**Γλωσσική εποπτεία:**  
Hafsa Bourkab
