---
name: Πανεπιστήμιο Jimma
country: Αιθιοπία
---

Abdisa Tesema - MSc. στην Αστροφυσική  
Bikila Teshome - MSc. στην Αστροφυσική  
Jifar Raya - MSc. στην Αστροφυσική  
Sena Bokona - MSc. στην Αστροφυσική  
Tolu Biressa - PhD στην Αστροφυσική
