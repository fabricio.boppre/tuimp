---
name: Associação Astrônoma Sirius
country: Argélia
---

**Tradutores:**  
Hafsa Bourkab  
Farah Derradji  
Khaoula Laggoune  
Echeima Amine-khodja  
Asma Lakroune  
Zeyneb Aissani  
Guergouri Hichem

**Supervisão científica:**  
Jamal Mimouni  
Hichem Guergouri

**Supervisão linguística:**  
Hafsa Bourkab
