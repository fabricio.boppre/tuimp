---
name: Georges Alecian
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: França
---

Georges Alecian é um astrofísico francês. Ele é diretor emérito de pesquisa no CNRS e trabalha no Observatório de Paris no campo da física estelar. Sua atividade de pesquisa se concentra na composição química das estrelas e, em particular, nos processos físicos que criam inhomogeneidades na distribuição dos elementos. Ele colabora com astrofísicos de vários países, principalmente da França, Canadá, Áustria e Armênia. Seus hobbies são fotografia, bricolage e gadgets de alta tecnologia. Sua esposa também é astrofísica. Eles têm uma filha, três netos e um número variável de gatos.
