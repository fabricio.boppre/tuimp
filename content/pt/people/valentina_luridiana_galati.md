---
name: Valentina Luridiana Galati
institution:
  name: Instituto de Astrofísica de Canárias
  slug: instituto_de_astrofisica_de_canarias
  country: Espanha
---

Astrofísica e tradutora freelancer.
