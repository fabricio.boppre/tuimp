---
name: Gloria Delgado Inglada
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: México
---

[Gloria](http://www.gloriadelgadoinglada.com/) nasceu em Madrid, Espanha, uma cidade que ela adora. Ela se mudou para México em 2004 para estudar astronomia e desde então mora nesse país, primeiro em Puebla e agora na Cidade do México. Sua pesquisa é principalmente em nebulosas ionizadas, e ela as usa para aprender sobre a produção e evolução de elementos em galáxias. Gloria gosta de ler e escrever, viajar, e passear e brincar por horas com o seu maravilhoso cão Anequi. Ela é muito ativa em divulgação científica, incluindo [podcasts](http://aficionporlaciencia.blogspot.mx/) sobre ciência, a edição do boletim "El Búho azul", e na escrita de artigos de divulgação.
