---
name: Mimoza Hafizi
institution:
  name: Universiteti i Tiranës
  slug: universiteti_i_tiranes
  country: Albânia
---

Mimoza nasceu em Shkodra, uma bela cidade histórica na parte norte da Albânia. Ela obteve seu doutorado em física na Universidade de Paris 7 e atualmente é professora titular de Astrofísica na Universidade de Tirana. Às vezes, com seus alunos, ela faz observações de planetas, estrelas e galáxias com seu próprio telescópio, das quais participam com entusiasmo várias pessoas da cidade de Tirana. Ela gosta de ler, ouvir música e cantar, caminhar quilômetros à beira-mar e viajar por todo o mundo com o marido. Além disso, ela é muito ativa na vida política de seu país. Suas duas filhas são suas melhores amigas e conselheiras. Ela se dedica especialmente a planetas extrasolares e, ao mesmo tempo, participa de uma equipe científica dedicada a descobrir o universo muito antigo com surtos de raios gama, os eventos mais enérgicos do Universo.
