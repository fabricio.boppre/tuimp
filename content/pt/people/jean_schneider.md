---
name: Jean Schneider
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: França
---

Depois de trabalhar em microfísica, Jean Schneider voltou-se para a astronomia, que traz mais imaginação à cena. De galáxias, ele se voltou a exoplanetas e à busca de vida ao redor de outras estrelas. Ele começou a detectar exoplanetas com o satélite CoRoT e propôs um método para buscar planetas com dois sóis. Ele também está interessado em filosofia da linguagem e na questão das relações entre corpo e mente. Como hobby, ele começou a pintar aquarela.
