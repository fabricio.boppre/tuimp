---
name: Grażyna Stasińska
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: França
---

Grażyna nasceu na França de pais poloneses e morou em Paris a maior parte da sua vida. Ela é astrofísica e membro do Observatório de Paris. Ela é especialista em nebulosas e galáxias. Trabalhou principalmente com astrofísicos poloneses, mexicanos, brasileiros e espanhóis, que se tornaram amigos próximos. Ela tem muitos passatempos, como aprender línguas e cozinhar comidas de todo o mundo. Ela também gosta de ler livros e ouvir música de todas as partes do mundo. Seu marido também é astrofísico. Eles têm uma filha, dois netos e três gatos.
