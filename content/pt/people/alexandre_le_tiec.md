---
name: Alexandre Le Tiec
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Alexandre é um investigador do CNRS que trabalha no Observatório de Paris no domínio da astrofísica de altas energias. A sua investigação centra-se principalmente na física dos buracos negros e nas ondas gravitacionais geradas por pares de estrelas compactas. Recentemente, desenvolveu uma paixão pela história da vida na Terra e da nossa espécie, o Homo sapiens. Paralelamente à sua investigação em astrofísica, está a levar a cabo uma investigação nas áreas das ciências naturais, humanas e sociais, com o objetivo de encontrar as raízes do Antropoceno. Nos seus tempos livres, lê livros, ouve música e dá passeios, por vezes ao mesmo tempo.
