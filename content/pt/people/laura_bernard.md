---
name: Laura Bernard
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: France
---
Laura Bernard nasceu e cresceu em Lozère, uma bela região de meia montanha no sul da França. Ela é pesquisadora do CNRS e trabalha no Observatório de Paris. É especialista em ondas gravitacionais e está trabalhando em alternativas à relatividade geral. Ela está contribuindo na preparaçâo de futuras missões para detectar ondas gravitacionais. Em seu tempo livre, ela também pratica atletismo em alto nível e participa de corridas de longa distância. Quando não está correndo, pode ser encontrada imersa em um livro, no cinema ou recarregando as baterias em Lozère.
