---
name: Julieta Fierro
institution:
  name: Universidad Nacional Autónoma de México
  slug: universidad_nacional_autonoma_de_mexico
  country: México
---

Julieta Fierro trabalha no Instituto de Astronomia da Universidade Nacional Autônoma do México e se dedica à popularização da ciência. Ela trabalhou em museus, projetou oficinas científicas, escreveu livros e artigos, produziu séries de televisão e deu palestras. Ela é participante freqüente de programas de rádio e televisão. Ela considera a popularização da ciência como parte da educação informal, ou seja, da aprendizagem ao longo da vida. Em seu tempo livre, ela gosta de ler e tocar flauta.
