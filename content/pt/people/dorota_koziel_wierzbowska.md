---
name: Dorota Kozieł-Wierzbowska
institution:
  name: Universytet Jagielloński
  slug: uniwersytet_jagiellonski
  country: Polônia
---

Dorota nasceu em Gorlice, e passou a infância em uma aldeia perto de Biecz, na Pequena Polônia, onde ela podia sem qualquer restrição desfrutar de um céu escuro adornado com a Via Láctea. Ela se mudou para Cracóvia para estudar física e astronomia, e desde então ela está ligada a esta cidade e arredores. Ela está estudando galáxias ativas, particularmente rádio galáxias, embora principalmente na luz visível. Ela ainda está encantada com o céu e, sempre que o tempo, os alunos e os comitês de alocação de tempo de telescópio permitem, ela faz observações de estrelas e galáxias. Em casa, ela tenta tomar controle do doce caos introduzido em sua vida por seus filhos gêmeos.
