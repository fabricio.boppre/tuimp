---
name: Natalia Vale Asari
institution:
  name: Universidade Federal de Santa Catarina
  slug: universidade_federal_de_santa_catarina
  country: Brasil
---

Natalia nasceu em Brasília, Brasil. Ela gosta de viajar para experimentar todos os tipos de comida, e também todos os tipos de canetas, lápis e papéis. Ela gosta de ter música e literatura sempre à mão. Seu marido costuma surpreendê-la com ainda mais música, livros e filmes, e eles possuem alguns pássaros livres em ninhos em sua casa, como seus animais de estimação. Ela morou em Brasília e Natal (Brasil), em Paris (França) e Cambridge (Inglaterra). Ela agora vive em Florianópolis e ensina Físca na Universidade Federal de Santa Catarina. Sua pesquisa é na área de astrofísica das galáxias.
