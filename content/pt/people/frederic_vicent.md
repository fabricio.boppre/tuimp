---
name: Frédéric Vicent
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: França
---

Frédéric Vicent é pesquisador do Observatório de Paris. Entre outras coisas, ele trabalha com buracos negros. Ele é membro da vasta colaboração do Event Horizon Telescope, que publicou a primeira imagem da sombra de um buraco negro em 2019.
