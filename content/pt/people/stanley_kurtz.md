---
name: Stanley Kurtz
institution:
  name: Universidad Nacional Autónoma de México, Morelia
  slug: universidad_nacional_autonoma_de_mexico
  country: México
---

Stan tem doutorado em física pela Universidade de Wisconsin, com especialização em astrofísica e radio astronomia. Atualmente é professor titular do Instituto de Radio Astronomia e Astrofísica da Universidade Nacional Autônoma do México. Sua pesquisa é na área de formação de estrelas maciças e os efeitos dessas estrelas no meio interestelar. Ele orientou projetos de tese desde a graduação até o doutorado, e regularmente ensina cursos de graduação em física e pós-graduação em astronomia. Em outra época da sua vida ele já foi um fotógrafo, um fazendeiro, e um entusiasta de carros de alta performance. Se ele tivesse um tempo livre, ele poderia usá-lo para cozinhar, mexer no jardim, fazer cerveja, ler amplamente (preferencialmente bebendo cerveja), aprender outras línguas, e tirar uma soneca todos os dias.
