---
name: Fabricio Chiquio Boppré
---

Fabricio nasceu e vive na Ilha de Santa Catarina, na cidade de Florianópolis, ao sul do Brasil. Sua paixão principal é a música, mas ele também adora viajar (para assistir concertos) e cozinhar de noite em casa (enquanto ouve sua coleção de discos). Sendo casado com uma astrofísica, ele vive perguntando para ela o que havia antes do Big Bang. Ele considera que ela ainda não o respondeu satisfatoriamente, e por isso vai continuar insistindo na pergunta. Literatura, video-game, filmes de terror, nadar e ir para a praia são outras de suas ocupações diárias. Quando sobra tempo, ele trabalha construindo web-sites. Para saber mais sobre ele, clique [aqui](http://www.fabricioboppre.net/).
