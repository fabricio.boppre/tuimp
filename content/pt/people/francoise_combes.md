---
name: Françoise Combes
institution:
  name: Observatoire de Paris
  slug: observatoire_de_paris
  country: França
---

Françoise Combes é uma astrofísica francesa muito famosa. Ela trabalha no Observatório de Paris. Os seus numerosos trabalhos sobre a formação e evolução das galáxias em um contexto cosmológico, tanto do ponto de vista teórico como observacional, valeram-lhe vários prêmios internacionais, bem como a medalha de ouro do Centro Nacional da Pesquisa Científica. Françoise costuma dar palestras abertas a todos e escrever vários livros para o público em geral. Ela gosta muito de viajar. Sua vida familiar também é muito rica, com três filhos e sete netos.
