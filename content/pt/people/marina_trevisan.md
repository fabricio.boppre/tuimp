---
name: Marina Trevisan
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brasil
---

Marina nasceu em Curitiba, mas cresceu em Itajaí e já morou em várias outras cidades do Brasil. Já viveu em Londres (Inglaterra) e em Paris (França) também. Hoje, ela está em Porto Alegre e é professora e pesquisadora na Universidade Federal do Rio Grande do Sul. Sua pesquisa está relacionada com formação e evolução de galáxias. Ela é apaixonada por Astronomia desde pequena e até hoje perde o fôlego ao ver um céu escuro e estrelado. Ela é conhecida entre os amigos por ser louca por gatos, mas só tem quatro por falta de espaço. Ela é casada e tem uma filha.
