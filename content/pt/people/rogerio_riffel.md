---
name: Rogério Riffel
institution:
  name: Universidade Federal do Rio Grande do Sul
  slug: universidade_federal_do_rio_grande_do_sul
  country: Brasil
---

Rogério nasceu em Alegria, uma pequena cidade do interior do Rio Grande do Sul, no Brazil. Sempre gostou de ver o céu e tentar entendê-lo nas caminhadas na madrugada rumo ao colégio com seu irmão, também astrofísico. Caminhava 12 km por dia para ir à aula. É licenciado em física, com mestrado e doutorado em astrofísica. Hoje é professor e faz pesquisa científica na Universidade Federal do Rio Grande do Sul. Sua pesquisa está relacionada com galáxias ativas e seus buracos negros supermassivos. Gosta de admirar o céu, jogar futebol, cultivar alimentos, fazer cervejas e tomar chimarrão. É casado e tem dois filhos.
