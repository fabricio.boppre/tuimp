---
name: Sylvia Ekström
institution:
  name: Observatoire de Genève
  slug: observatoire_de_geneve
  country: Suiça
---
Sylvia nasceu em Estocolmo e cresceu em Genebra. Formou-se como parteira e trabalhou numa sala de partos durante 11 anos antes de deixar tudo para trás para se dedicar à astrofísica depois de ver o cometa Hale-Bopp. Especialista em física estelar, é também diretora de comunicação do Departamento de Astronomia da Universidade de Genebra. Casada e com dois gatos, passa o seu tempo livre a cantar e a tocar violoncelo, dando regularmente concertos.
